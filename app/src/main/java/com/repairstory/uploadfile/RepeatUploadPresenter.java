package com.repairstory.uploadfile;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.repairstory.utils.AppConstants;
import com.repairstory.utils.Helper;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadTask;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;


public class RepeatUploadPresenter {


    private String TAG = RepeatUploadPresenter.class.getSimpleName();

    private Context mContext;

    public RepeatUploadPresenter(Context mcontext) {
        this.mContext = mcontext;
    }


    public void hitUploadContent(Context mcontext, List<String> uploadItem, Integer storyId, String mediaStep, ArrayList<String> deletedServerMediaList) {

        createNotificationChannel(mContext);

        String deletedMedia = "[]";

        if (deletedServerMediaList.size() > 0) {

//            for (int i = 0; i < deletedServerMediaList.size(); i++) {
//                deletedServerMediaList.set(i, deletedServerMediaList.get(deletedServerMediaList.get(i).lastIndexOf("\\/") + 1));
//            }

            JSONArray deletedMediaArray = new JSONArray(deletedServerMediaList);
            deletedMedia = deletedMediaArray.toString();
        }
        try {
            Log.d(TAG, "hitUploadContent: " + String.valueOf(storyId) + "   " + deletedMedia);

            MultipartUploadRequest uploadRequest = new MultipartUploadRequest(mcontext, "" + storyId, AppConstants.mBaseUrl + "stories/" + storyId + "/medias")
                    .addHeader("Authorization", Helper.getAccesstoken(mcontext))
                    .addParameter("step", mediaStep)
                    .addParameter("deleted_media", deletedMedia);

            for (int i = 0; i < uploadItem.size(); i++) {
                if (!uploadItem.get(i).startsWith("http"))
                    uploadRequest.addFileToUpload(uploadItem.get(i), "media[]");
            }

            uploadRequest.setNotificationConfig(getNotificationConfig(mcontext, "" + storyId, "Media Uploading"))
                    .setMaxRetries(5)
                    .startUpload();
        } catch (Exception exc) {
            exc.printStackTrace();
            Toast.makeText(mcontext, exc.getMessage(), Toast.LENGTH_LONG).show();
        }


    }


    public UploadNotificationConfig getNotificationConfig(Context context, final String uploadId, String title) {
        UploadNotificationConfig config = new UploadNotificationConfig();

        PendingIntent clickIntent = PendingIntent.getActivity(
                context, 1, new Intent(), PendingIntent.FLAG_UPDATE_CURRENT);

        config.setTitleForAllStatuses(title)
                .setRingToneEnabled(false)
                .setClickIntentForAllStatuses(clickIntent)
                .setClearOnActionForAllStatuses(true);

        return config;
    }


    public void createNotificationChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(UploadTask.CHANNEL_ID,
                    "Repairstory Service Channel",
                    NotificationManager.IMPORTANCE_NONE
            );
            NotificationManager manager = context.getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

}
