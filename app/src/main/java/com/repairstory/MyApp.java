package com.repairstory;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;

import com.repairstory.controller.IDataManager;
import com.repairstory.di.components.ApplicationComponent;
import com.repairstory.di.modules.ApplicationModule;
import com.repairstory.di.components.DaggerApplicationComponent;

import javax.inject.Inject;


public class MyApp extends Application {
	
	ApplicationComponent appComponent;

	@Inject
	IDataManager mIDataManager;
	private static MyApp mInstance;


	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;

		initializeInjector();

		// Setup handler for uncaught exceptions.
		Thread.setDefaultUncaughtExceptionHandler (this::handleUncaughtException);
	}
	
	private void initializeInjector(){
		appComponent = DaggerApplicationComponent.builder()
				.applicationModule(new ApplicationModule(this))
				.build();
		
		appComponent.inject(this);
		
	}
	
	public ApplicationComponent getAppComponent(){
		return appComponent;
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}

	public void handleUncaughtException (Thread thread, Throwable e)
	{
		Thread.UncaughtExceptionHandler uch = Thread.getDefaultUncaughtExceptionHandler();
		e.printStackTrace(); // not all Android versions will print the stack trace automatically

		System.out.println("UncaughtException is handled!");

		System.exit(-1);
	}

	public static synchronized MyApp getInstance() {
		return mInstance;
	}

}