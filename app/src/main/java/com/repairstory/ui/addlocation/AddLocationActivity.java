package com.repairstory.ui.addlocation;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.repairstory.R;
import com.repairstory.baseactivities.BaseActivity;
import com.repairstory.models.logins.LoginResponseModel;
import com.repairstory.ui.profile.ProfileActivity;
import com.repairstory.utils.AppConstants;
import com.repairstory.utils.customviews.CustomButton;
import com.repairstory.utils.customviews.CustomTextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;


public class AddLocationActivity extends BaseActivity implements AddLocationMvpView, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener,
        GoogleMap.OnCameraChangeListener {


    @BindView(R.id.progressRL)
    RelativeLayout progressRL;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.myLocation)
    ImageView myLocation;
    @BindView(R.id.addressTV)
    CustomTextView addressTV;
    @BindView(R.id.selectLocation)
    LinearLayout selectLocation;
    @BindView(R.id.bottomRL)
    RelativeLayout bottomRL;
    @BindView(R.id.main)
    RelativeLayout main;
    @BindView(R.id.addDetailsBtn)
    CustomButton addDetailsBtn;

    private String TAG = AddLocationActivity.class.getSimpleName();

    public static final int REQUEST_CODE = 500;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static final int ZOOM_ID = 0x1;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private GoogleMap googleMap;
    private double longitude, latitude;
    private Geocoder mGeocoder;

    private String modeType;

    @Inject
    AddLocationMvpPresenter<AddLocationMvpView> mPresenter;


    private ArrayList<Address> address = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getActivityComponent().inject(AddLocationActivity.this);
        ButterKnife.bind(this);
        mPresenter.onAttach(AddLocationActivity.this);

        modeType = getIntent().getExtras().getString(AppConstants.MODE_TYPE);
        if (modeType.equalsIgnoreCase(AppConstants.MODE_TYPE_CONSTANTS.EDIT)) {
            latitude = getIntent().getExtras().getDouble(AppConstants.LATITUDE);
            longitude = getIntent().getExtras().getDouble(AppConstants.LONGITUDE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        addGoogleMap();
        buildGoogleApiClient();
        initUI();

    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_add_location;
    }

    @Override
    protected void initUI() {
        initErrorViews(findViewById(R.id.error_views_layout));

    }


    @Override
    public void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();

    }

    @Override
    protected void handleNoInternet() {

    }

    @Override
    protected void handleSlowInternet() {

    }

    @Override
    protected void handleServerError() {


    }


    @Override
    public void onSuccess(Response<LoginResponseModel> response) {

    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }


    @OnClick({R.id.back, R.id.myLocation, R.id.addDetailsBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.myLocation:
                initiateCurrentLocation();
                break;
            case R.id.addDetailsBtn:
                if (latitude != 0 && longitude != 0 && address.size()>0) {
                    setDataBack();
                } else {
                    showToast("Choose Location First");
                }

                break;
        }
    }


    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        googleMap.setMyLocationEnabled(true);
//                        isPermissionDenied = false;
                        initiateCurrentLocation();
                    }
                } else {
                    LatLng latLng = new LatLng(latitude, longitude);
                    googleMap.animateCamera(CameraUpdateFactory.zoomIn());
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(8), 5000, null);
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(latLng)       // Sets the center of the map to Mountain View
                            .zoom(18)                   // Sets the zoom
                            .bearing(0)                // Sets the orientation of the camera to east
                            .tilt(30)             // Sets the tilt of the camera to 30 degrees
                            .build(); // .tilt(30)                    // Creates a CameraPosition from the builder
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    progressRL.setVisibility(View.GONE);
                    Toast.makeText(this, "Permission Denied - Some features would not be available. Please go to Setting > Apps > LYQUOA > Permissions, and enable Location.", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }


    /*get google map */
    private void addGoogleMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        View zoomControls = mapFragment.getView().findViewById(ZOOM_ID);
        if (zoomControls != null && zoomControls.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams params_zoom = (RelativeLayout.LayoutParams) zoomControls.getLayoutParams();
            // Align it to - parent top|left
            params_zoom.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            params_zoom.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            // Update margins, set to 10dp
            final int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50, getResources().getDisplayMetrics());
            params_zoom.setMargins(margin, margin, margin, margin);

        }

    }

    @Override
    public void onMapReady(GoogleMap gmap) {
        googleMap = gmap;
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.setOnCameraChangeListener(this);

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    private void initiateCurrentLocation() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, AddLocationActivity.this);
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        initiateCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        //Place current location marker
        LatLng latLng;
        latLng = new LatLng(location.getLatitude(), location.getLongitude());
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, AddLocationActivity.this);
        }
        latitude = latLng.latitude;
        longitude = latLng.longitude;

        googleMap.animateCamera(CameraUpdateFactory.zoomIn());
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(8), 5000, null);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)       // Sets the center of the map to Mountain View
                .zoom(18)                   // Sets the zoom
                .bearing(0)                // Sets the orientation of the camera to east
                .tilt(30)             // Sets the tilt of the camera to 30 degrees
                .build(); // .tilt(30)                    // Creates a CameraPosition from the builder
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


        try {
            progressRL.setVisibility(View.GONE);
        } catch (Exception e) {
        }
    }


    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        if (mConnectionDetector.isInternetConnected()) {
            LatLng center = googleMap.getCameraPosition().target;
            if (center != null) {
                latitude = center.latitude;
                longitude = center.longitude;
                getAddress();

            }
        } else {
            showToast("No Internet Connection");
        }
    }


    private void getAddress() {

        if (latitude != 0.0 && longitude != 0.0) {
            mGeocoder = new Geocoder(AddLocationActivity.this, Locale.getDefault());

            try {
                address.clear();
                address.addAll(mGeocoder.getFromLocation(latitude, longitude, 1)); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                Log.d(TAG, "getAddress: " + latitude + " " + longitude + " " + address);
                if (address != null && address.size() > 0) {
                    //String mUserAddLine = address.get(0).getAddressLine(0);//house no. // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String mUserAddLine = address.get(0).getAddressLine(0);
                    mUserAddLine = mUserAddLine.replace("[", "");
                    mUserAddLine = mUserAddLine.replace("]", "");

                    addressTV.setText(mUserAddLine);
                    Log.d(TAG, "getAddress: " + latitude + " " + longitude + " " + mUserAddLine);
//                mGoogleApiClient.disconnect();

                    addDetailsBtn.setVisibility(View.VISIBLE);
                }

            } catch (IOException e) {

            }
        }
    }


    public static void startForResult(ProfileActivity context, double latitude, double longitude, String modeType, int requestCode) {
        Intent intent = new Intent(context, AddLocationActivity.class);
        intent.putExtra(AppConstants.MODE_TYPE, modeType);
        intent.putExtra(AppConstants.LATITUDE, latitude);
        intent.putExtra(AppConstants.LONGITUDE, longitude);
        context.startActivityForResult(intent, requestCode);
    }

    private void setDataBack() {
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(AppConstants.LOCATION_DATA, address);
        intent.putExtra(AppConstants.LATITUDE, latitude);
        intent.putExtra(AppConstants.LONGITUDE, longitude);
        setResult(RESULT_OK, intent);
        finish();
    }

}
