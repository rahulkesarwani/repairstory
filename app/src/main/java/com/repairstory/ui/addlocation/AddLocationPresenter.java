package com.repairstory.ui.addlocation;

import com.repairstory.baseactivities.base.BasePresenter;
import com.repairstory.controller.IDataManager;

/**
 *
 */
public class AddLocationPresenter<V extends AddLocationMvpView> extends BasePresenter<V> implements AddLocationMvpPresenter<V> {


    public AddLocationPresenter(IDataManager IDataManager) {
        super(IDataManager);
    }


//    @Override
//    public void hitEditLocationApi(LocationModel locationModel) {
//        getIDataManager().getCompositeDisposable().add(
//                getIDataManager().getApiInterface().hitLocationEditApi(locationModel.getId(), locationModel)
//                        .subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .subscribe(commonResponse -> {
//
//                            getMvpView().onSuccessEdit(commonResponse);
//                        }, throwable -> {
//
//                            getMvpView().onFailure(throwable);
//                        })
//        );
//    }


}
