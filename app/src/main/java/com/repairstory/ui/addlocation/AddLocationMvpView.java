package com.repairstory.ui.addlocation;


import com.repairstory.baseactivities.base.MvpView;
import com.repairstory.models.logins.LoginResponseModel;
import retrofit2.Response;


public interface AddLocationMvpView extends MvpView {

    void onSuccess(Response<LoginResponseModel> response);

    void onFailure(Throwable error);

}
