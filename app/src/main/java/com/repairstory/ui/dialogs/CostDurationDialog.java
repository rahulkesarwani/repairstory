package com.repairstory.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.widget.ArrayAdapter;

import androidx.appcompat.widget.AppCompatSpinner;

import com.repairstory.R;
import com.repairstory.utils.MessagesUtils;
import com.repairstory.utils.customviews.CustomButton;
import com.repairstory.utils.customviews.CustomEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by rahul
 */

public class CostDurationDialog extends Dialog {


    @BindView(R.id.costET)
    CustomEditText costET;
    @BindView(R.id.durationET)
    CustomEditText durationET;
    @BindView(R.id.unitSpinner)
    AppCompatSpinner unitSpinner;
    @BindView(R.id.doneBtn)
    CustomButton doneBtn;
    private Context context;

    private String TAG = CostDurationDialog.class.getCanonicalName();
    private CostDurationListener okListerner;

    public CostDurationDialog(Context context) {
        super(context, R.style.mydialog_90);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_cost_duration);
        ButterKnife.bind(this);
        initUnitSpinner();
    }

    private void initUnitSpinner() {
        String[] units = context.getResources().getStringArray(R.array.duration_unit);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, units);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        unitSpinner.setAdapter(dataAdapter);

    }

    private boolean validateFields() {

        if (costET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(context, "Enter Cost");
            return false;
        } else if (durationET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(context, "Enter Duration");
            return false;
        } else {
            return true;
        }
    }

    @OnClick(R.id.doneBtn)
    public void onViewClicked() {
        if (validateFields()) {
            okListerner.onDoneClicked(costET.getText().toString().trim(), durationET.getText().toString().trim() + " " + unitSpinner.getSelectedItem().toString());
            dismiss();
        }
    }


    public interface CostDurationListener {
        void onDoneClicked(String cost, String duration);
    }

    public void setCostDurationListener(CostDurationListener listener) {
        okListerner = listener;
    }


}