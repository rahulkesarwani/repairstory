package com.repairstory.ui.login;

import com.repairstory.baseactivities.base.MvpPresenter;
import com.repairstory.models.logins.LoginSignupRequestModel;

/**
 * Created by rahul on 6/14/2018.
 */

public interface LoginMvpPresenter<V extends LoginMvpView>  extends MvpPresenter<V> {
    void hitLogin(LoginSignupRequestModel loginRequestModel);
}