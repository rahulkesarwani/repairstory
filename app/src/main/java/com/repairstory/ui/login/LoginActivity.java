package com.repairstory.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.repairstory.R;
import com.repairstory.baseactivities.BaseActivity;
import com.repairstory.models.logins.LoginSignupRequestModel;
import com.repairstory.models.logins.LoginResponseModel;

import com.repairstory.ui.signup.SignupActivity;
import com.repairstory.ui.home.HomeActivity;
import com.repairstory.utils.AppConstants;
import com.repairstory.utils.CheckNullable;
import com.repairstory.utils.Helper;
import com.repairstory.utils.MessagesUtils;
import com.repairstory.utils.customviews.CustomButton;
import com.repairstory.utils.customviews.CustomEditText;
import com.repairstory.utils.customviews.CustomTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;


public class LoginActivity extends BaseActivity implements LoginMvpView {

    @BindView(R.id.userNameET)
    CustomEditText userNameET;
    @BindView(R.id.passwordET)
    CustomEditText passwordET;
    @BindView(R.id.loginBtn)
    CustomButton loginBtn;
    @BindView(R.id.crateAccBtn)
    CustomTextView crateAccBtn;

    private String TAG = LoginActivity.class.getSimpleName();

    @Inject
    LoginMvpPresenter<LoginMvpView> mPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mySharedPreferences.deleteSingleKey(AppConstants.ACCESS_TOKEN);
        getActivityComponent().inject(LoginActivity.this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(LoginActivity.this);

        initUI();

    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void initUI() {
        initErrorViews(findViewById(R.id.error_views_layout));
        crateAccBtn.setText(Helper.fromHtml(getString(R.string.signup_up_text)));
    }


    private void hitLoginApi() {

        if (checkBeforeApiHit()) {

            LoginSignupRequestModel loginRequestModel = new LoginSignupRequestModel();
            loginRequestModel.setIdentity(userNameET.getText().toString().trim());
            loginRequestModel.setPassword(passwordET.getText().toString().trim());

            mPresenter.hitLogin(loginRequestModel);
        }
    }


    @Override
    public void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();

    }

    @Override
    protected void handleNoInternet() {

        hitLoginApi();
    }

    @Override
    protected void handleSlowInternet() {

        hitLoginApi();
    }

    @Override
    protected void handleServerError() {
        hitLoginApi();
    }


    @Override
    public void onSuccess(Response<LoginResponseModel> response) {

        hideProgressBar();

        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(LoginActivity.this, errorResponse);
        } else if (isResponseOK((short) response.code())) {

            if (response.body().getSuccess()) {
                mySharedPreferences.putString(AppConstants.ACCESS_TOKEN, response.body().getData().getAuthToken());
                mySharedPreferences.setObj(AppConstants.LOGIN_DATA, response.body().getData());

                startActivity(HomeActivity.class);
                finish();

            } else {
                MessagesUtils.showToastError(this, response.body().getMessage());
            }
        }
    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @OnClick({R.id.loginBtn, R.id.crateAccBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.loginBtn:

                if (validateFields())
                    hitLoginApi();

                break;

            case R.id.crateAccBtn:
                startActivity(SignupActivity.class);
                break;
        }

    }

    private boolean validateFields() {
        if (userNameET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Username or Email");
            return false;
        } else if (passwordET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Password");
            return false;
        } else {
            return true;
        }
    }
}
