package com.repairstory.ui.login;

import com.repairstory.baseactivities.base.BasePresenter;
import com.repairstory.controller.IDataManager;
import com.repairstory.models.logins.LoginSignupRequestModel;
import com.repairstory.models.logins.LoginResponseModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 *
 * */
public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<V> implements LoginMvpPresenter<V> {


    public LoginPresenter(IDataManager IDataManager) {
        super(IDataManager);
    }

    @Override
    public void hitLogin(LoginSignupRequestModel loginRequestModel) {

        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitLoginApi(loginRequestModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {
                            getMvpView().onSuccess(commonResponse);
                        }, throwable -> {

                            getMvpView().onFailure(throwable);
                        })
        );

    }
}
