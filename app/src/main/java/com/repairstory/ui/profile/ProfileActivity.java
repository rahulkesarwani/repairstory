package com.repairstory.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.mynameismidori.currencypicker.CurrencyPicker;
import com.mynameismidori.currencypicker.CurrencyPickerListener;
import com.repairstory.R;
import com.repairstory.baseactivities.LocationImagePickerBaseActivity;
import com.repairstory.models.logins.Datum;
import com.repairstory.models.logins.LoginResponseModel;
import com.repairstory.ui.addlocation.AddLocationActivity;
import com.repairstory.ui.login.LoginActivity;
import com.repairstory.utils.AppConstants;
import com.repairstory.utils.CheckNullable;
import com.repairstory.utils.GlideUtils;
import com.repairstory.utils.Helper;
import com.repairstory.utils.MessagesUtils;
import com.repairstory.utils.customviews.CustomEditText;
import com.repairstory.utils.customviews.CustomTextView;
import com.vikktorn.picker.City;
import com.vikktorn.picker.CityPicker;
import com.vikktorn.picker.Country;
import com.vikktorn.picker.CountryPicker;
import com.vikktorn.picker.OnCityPickerListener;
import com.vikktorn.picker.OnCountryPickerListener;
import com.vikktorn.picker.OnStatePickerListener;
import com.vikktorn.picker.State;
import com.vikktorn.picker.StatePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;


public class ProfileActivity extends LocationImagePickerBaseActivity implements ProfileMvpView, OnStatePickerListener, OnCountryPickerListener, OnCityPickerListener {


    @BindView(R.id.backIV)
    ImageView backIV;
    @BindView(R.id.logoutIV)
    ImageView logoutIV;
    @BindView(R.id.top)
    RelativeLayout top;
    @BindView(R.id.profileIV)
    CircleImageView profileIV;
    @BindView(R.id.shopNameET)
    CustomEditText shopNameET;
    @BindView(R.id.emailET)
    CustomEditText emailET;
    @BindView(R.id.mobileNoET)
    CustomEditText mobileNoET;
    @BindView(R.id.websiteET)
    CustomEditText websiteET;
    @BindView(R.id.serviceTagsET)
    MultiAutoCompleteTextView serviceTagsET;
    @BindView(R.id.selectAddressMap)
    CustomEditText selectAddressMap;
    @BindView(R.id.countryET)
    CustomEditText countryET;
    @BindView(R.id.stateET)
    CustomEditText stateET;
    @BindView(R.id.cityET)
    CustomEditText cityET;
    @BindView(R.id.localityET)
    CustomEditText localityET;
    @BindView(R.id.subLocalityET)
    CustomEditText subLocalityET;
    @BindView(R.id.currencyET)
    CustomEditText currencyET;
    @BindView(R.id.bioET)
    CustomEditText bioET;
    @BindView(R.id.updateProfileBtn)
    CustomTextView updateProfileBtn;
    private String TAG = ProfileActivity.class.getSimpleName();

    @Inject
    ProfileMvpPresenter<ProfileMvpView> mPresenter;

    private Datum userData;
    private CountryPicker countryPicker;
    private StatePicker statePicker;
    private CityPicker cityPicker;


    // arrays of state object
    public static List<State> stateObject;
    // arrays of city object
    public static List<City> cityObject;

    private double latitude, longitude;
    private String[] serviceTags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(ProfileActivity.this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(ProfileActivity.this);

        initErrorViews(findViewById(R.id.error_views_layout));
        userData = (Datum) mySharedPreferences.getObj(AppConstants.LOGIN_DATA, Datum.class);
        initUI();

    }


    @Override
    protected int getActivityLayout() {
        return R.layout.activity_profile;
    }

    @Override
    protected void initUI() {
        stateObject = new ArrayList<>();
        cityObject = new ArrayList<>();
        try {
            getStateJson();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            getCityJson();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        initServiceTags();

        countryPicker = new CountryPicker.Builder().with(this).listener(this).build();
        renderData();
    }


    private void initServiceTags() {

        serviceTags = getResources().getStringArray(R.array.service_tags);

        ArrayAdapter<String> versionNames = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, serviceTags);
        serviceTagsET.setAdapter(versionNames);
        serviceTagsET.setThreshold(1);
        serviceTagsET.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
    }

    @Override
    public void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();

    }

    @Override
    protected void handleNoInternet() {
        hitProfileUpdateApi();

    }

    @Override
    protected void handleSlowInternet() {
        hitProfileUpdateApi();

    }

    @Override
    protected void handleServerError() {
        hitProfileUpdateApi();
    }


    @Override
    public void onSuccess(Response<LoginResponseModel> response) {
        hideProgressBar();

        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(ProfileActivity.this, errorResponse);
        } else if (isResponseOK((short) response.code())) {

            if (response.body().getSuccess()) {

                userData.setLogo(response.body().getData().getLogo());
                mySharedPreferences.setObj(AppConstants.LOGIN_DATA, userData);

                MessagesUtils.showToastInfo(ProfileActivity.this, "Profile Updated Successfully!");
                finish();

            } else {
                showToast(response.body().getMessage());
            }
        }

    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }


    private void renderData() {

        if (userData == null)
            return;
        GlideUtils.loadImage(this, userData.getLogo(), profileIV, R.drawable.ic_profile);
        shopNameET.setText(CheckNullable.checkNullable(userData.getName()));
        emailET.setText(CheckNullable.checkNullable(userData.getEmail()));
        mobileNoET.setText(CheckNullable.checkNullable(userData.getPhone()));
        websiteET.setText(CheckNullable.checkNullable(userData.getWebsite()));

        selectAddressMap.setText(CheckNullable.checkNullable(userData.getAddress()));
        countryET.setText(CheckNullable.checkNullable(userData.getCountry()));
        cityET.setText(CheckNullable.checkNullable(userData.getCity()));
        stateET.setText(CheckNullable.checkNullable(userData.getState()));
        localityET.setText(CheckNullable.checkNullable(userData.getLocality()));
        subLocalityET.setText(CheckNullable.checkNullable(userData.getSubLocalityAddress()));
        currencyET.setText(CheckNullable.checkNullable(userData.getCurrency()));
        bioET.setText(CheckNullable.checkNullable(userData.getBio()));

        latitude = CheckNullable.checkNullable(userData.getLatitude());
        longitude = CheckNullable.checkNullable(userData.getLongitude());

        //need to modify
        if (userData.getServicesTags() == null)
            return;
        if (userData.getServicesTags().size() == 0)
            return;

        String tagStriing = userData.getServicesTags().toString().replaceAll("\\[", "").replaceAll("]", "");
        serviceTagsET.setText(tagStriing + ",");
    }


    private List<String> getSelectedTags() {
        List<String> selectedserviceTags = new ArrayList<>();
        if (!TextUtils.isEmpty(serviceTagsET.getText().toString().trim())) {
            String tagString = "";
            try {
                tagString = serviceTagsET.getText().toString().trim().substring(0, serviceTagsET.getText().toString().trim().lastIndexOf(","));
            } catch (Exception e) {
                tagString = serviceTagsET.getText().toString().trim();
            }
            String tags[] = tagString.split(",");
            selectedserviceTags = Arrays.asList(tags);
        }
        return selectedserviceTags;
    }


    private void hitProfileUpdateApi() {

        if (checkBeforeApiHit()) {
            MultipartBody.Builder builder = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("name", CheckNullable.checkNullable(userData.getName()))
                    .addFormDataPart("phone", CheckNullable.checkNullable(userData.getPhone()))
                    .addFormDataPart("website", CheckNullable.checkNullable(userData.getWebsite()))
                    .addFormDataPart("address", CheckNullable.checkNullable(userData.getAddress()))
                    .addFormDataPart("email", CheckNullable.checkNullable(userData.getEmail()))
                    .addFormDataPart("service_tags", getSelectedTagsPost())
                    .addFormDataPart("latitude", String.valueOf(CheckNullable.checkNullable(userData.getLatitude())))
                    .addFormDataPart("longitude", String.valueOf(CheckNullable.checkNullable(userData.getLongitude())))
                    .addFormDataPart("country", CheckNullable.checkNullable(userData.getCountry()))
                    .addFormDataPart("state", CheckNullable.checkNullable(userData.getState()))
                    .addFormDataPart("city", CheckNullable.checkNullable(userData.getCity()))
                    .addFormDataPart("locality", CheckNullable.checkNullable(userData.getLocality()))
                    .addFormDataPart("sub_locality_address", CheckNullable.checkNullable(userData.getSubLocalityAddress()))
                    .addFormDataPart("bio", CheckNullable.checkNullable(userData.getBio()))
                    .addFormDataPart("currency", CheckNullable.checkNullable(userData.getCurrency()))
                    .addFormDataPart("_method", "PUT");


            try {
                if (CheckNullable.checkNullable(userData.getLogo()).length() != 0) {
                    if (!(userData.getLogo().startsWith("http"))) {
                        File file = new File(userData.getLogo());
                        String fileName = userData.getLogo().substring(userData.getLogo().lastIndexOf("/") + 1);
                        builder.addFormDataPart("logo_pic", fileName, RequestBody.create(MediaType.parse("multipart/form-data"), file));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            MultipartBody body = builder.build();
            mPresenter.hitProfileUpdateApi(body);
        }
    }


    private String getSelectedTagsPost() {
        try {
            return serviceTagsET.getText().toString().trim().substring(0, serviceTagsET.getText().toString().trim().lastIndexOf(","));
        } catch (Exception e) {
            return serviceTagsET.getText().toString();
        }
    }

    private void openCurrencyDialog() {
        CurrencyPicker picker = CurrencyPicker.newInstance("Select Currency");  // dialog title
//        picker.setCurrenciesList();
        picker.setListener(new CurrencyPickerListener() {
            @Override
            public void onSelectCurrency(String name, String code, String symbol, int flagDrawableResID) {

                currencyET.setText(symbol);
                picker.dismiss();
            }
        });
        picker.show(getSupportFragmentManager(), "CURRENCY_PICKER");
    }


    @OnClick({R.id.countryET, R.id.stateET, R.id.cityET, R.id.currencyET, R.id.updateProfileBtn, R.id.selectAddressMap, R.id.profileIV, R.id.backIV, R.id.logoutIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.countryET:
                countryPicker.showDialog(getSupportFragmentManager());
                break;
            case R.id.cityET:
                if (cityPicker.equalCityObject.size() > 0) {
                    cityPicker.showDialog(getSupportFragmentManager());
                } else {
                    MessagesUtils.showToastError(this, "No City for this state");
                }
                break;
            case R.id.stateET:
                if (statePicker.equalStateObject.size() > 0)
                    statePicker.showDialog(getSupportFragmentManager());
                else
                    MessagesUtils.showToastError(this, "No State Found");
                break;
            case R.id.currencyET:
                openCurrencyDialog();
                break;
            case R.id.updateProfileBtn:
                if (validateFields()) {

                    setDataIntoLocalModel();
                    hitProfileUpdateApi();
                }

                break;
            case R.id.selectAddressMap:
                AddLocationActivity.startForResult(ProfileActivity.this, 0.0, 0.0, AppConstants.MODE_TYPE_CONSTANTS.CREATE, AddLocationActivity.REQUEST_CODE);
                break;
            case R.id.profileIV:
                pickFromGallery();
                break;
            case R.id.backIV:
                finish();
                break;
            case R.id.logoutIV:
                logoutUser();
                break;
        }
    }

    private void logoutUser() {
        mySharedPreferences.setObj(AppConstants.LOGIN_DATA, null);
        mySharedPreferences.putString(AppConstants.ACCESS_TOKEN, "");

        startActivity(LoginActivity.class);
        finishAffinity();
    }


    private void setDataIntoLocalModel() {
        userData.setName(shopNameET.getText().toString().trim());
        userData.setEmail(emailET.getText().toString().trim());
        userData.setPhone(mobileNoET.getText().toString().trim());
        userData.setWebsite(websiteET.getText().toString().trim());

        userData.setAddress(selectAddressMap.getText().toString().trim());
        userData.setCountry(countryET.getText().toString().trim());
        userData.setState(stateET.getText().toString().trim());
        userData.setCity(cityET.getText().toString().trim());
        userData.setLocality(localityET.getText().toString().trim());
        userData.setSubLocalityAddress(subLocalityET.getText().toString().trim());

        userData.setLatitude(latitude);
        userData.setLongitude(longitude);

        userData.setServicesTags(getSelectedTags());

        userData.setCurrency(currencyET.getText().toString().trim());
        userData.setBio(bioET.getText().toString().trim());
    }

    @Override
    public void onSelectCity(City city) {
        cityET.setText(city.getCityName());
    }

    @Override
    public void onSelectCountry(Country country) {
        countryET.setText(country.getName());
        statePicker.equalStateObject.clear();
        cityPicker.equalCityObject.clear();

        cityET.setText("");
        stateET.setText("");

        // GET STATES OF SELECTED COUNTRY
        for (int i = 0; i < stateObject.size(); i++) {
            // init state picker
            statePicker = new StatePicker.Builder().with(this).listener(this).build();
            State stateData = new State();
            if (stateObject.get(i).getCountryId() == country.getCountryId()) {

                stateData.setStateId(stateObject.get(i).getStateId());
                stateData.setStateName(stateObject.get(i).getStateName());
                stateData.setCountryId(stateObject.get(i).getCountryId());
                stateData.setFlag(country.getFlag());
                statePicker.equalStateObject.add(stateData);
            }
        }
    }

    @Override
    public void onSelectState(State state) {
        stateET.setText(state.getStateName());
        cityPicker.equalCityObject.clear();

        for (int i = 0; i < cityObject.size(); i++) {
            cityPicker = new CityPicker.Builder().with(this).listener(this).build();
            City cityData = new City();
            if (cityObject.get(i).getStateId() == state.getStateId()) {
                cityData.setCityId(cityObject.get(i).getCityId());
                cityData.setCityName(cityObject.get(i).getCityName());
                cityData.setStateId(cityObject.get(i).getStateId());

                cityPicker.equalCityObject.add(cityData);
            }
        }
    }


    // GET STATE FROM ASSETS JSON
    public List<State> getStateJson() throws JSONException {

        String json = null;
        try {
            InputStream inputStream = getAssets().open("states.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }


        JSONObject jsonObject = new JSONObject(json);
        JSONArray events = jsonObject.getJSONArray("states");
        for (int j = 0; j < events.length(); j++) {
            JSONObject cit = events.getJSONObject(j);
            State stateData = new State();

            stateData.setStateId(Integer.parseInt(cit.optString("id")));
            stateData.setStateName(cit.optString("name"));
            stateData.setCountryId(Integer.parseInt(cit.optString("country_id")));
            stateObject.add(stateData);
        }
        return stateObject;
    }


    // GET CITY FROM ASSETS JSON
    public List<City> getCityJson() throws JSONException {

        String json = null;
        try {
            InputStream inputStream = getAssets().open("cities.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
        }


        JSONObject jsonObject = new JSONObject(json);
        JSONArray events = jsonObject.getJSONArray("cities");
        for (int j = 0; j < events.length(); j++) {
            JSONObject cit = events.getJSONObject(j);
            City cityData = new City();

            cityData.setCityId(Integer.parseInt(cit.getString("id")));
            cityData.setCityName(cit.getString("name"));
            cityData.setStateId(Integer.parseInt(cit.getString("state_id")));
            cityObject.add(cityData);
        }
        return cityObject;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == AddLocationActivity.REQUEST_CODE) {
            ArrayList<Address> mapAddress = data.getParcelableArrayListExtra(AppConstants.LOCATION_DATA);

            latitude = data.getExtras().getDouble(AppConstants.LATITUDE);
            longitude = data.getExtras().getDouble(AppConstants.LONGITUDE);

            if (mapAddress == null)
                return;
            if (mapAddress.size() == 0)
                return;

            Log.d(TAG, "onActivityResult: " + mapAddress.toString());

            countryET.setText(CheckNullable.checkNullable(mapAddress.get(0).getCountryName()));
            cityET.setText(CheckNullable.checkNullable(mapAddress.get(0).getLocality()));
            stateET.setText(CheckNullable.checkNullable(mapAddress.get(0).getAdminArea()));
            localityET.setText(CheckNullable.checkNullable(mapAddress.get(0).getSubAdminArea()));

            String address = CheckNullable.checkNullable(mapAddress.get(0).getAddressLine(0));
            try {
                Log.d(TAG, "onActivityResult: " + address.indexOf(CheckNullable.checkNullable(mapAddress.get(0).getLocality())));
                subLocalityET.setText(address.substring(0, address.indexOf(CheckNullable.checkNullable(mapAddress.get(0).getLocality()))));
            } catch (Exception e) {
                subLocalityET.setText(address);

            }
            selectAddressMap.setText(CheckNullable.checkNullable(mapAddress.get(0).getAddressLine(0)));

        } else if (resultCode == RESULT_OK && requestCode == SELECT_PICTURE) {
            userData.setLogo(resultPath);
            GlideUtils.loadImage(this, resultPath, profileIV, R.drawable.ic_profile);
        }
    }

    @Override
    protected void gotCurrentLocation() {

    }

    private boolean validateFields() {
        if (TextUtils.isEmpty(userData.getLogo())) {
            MessagesUtils.showToastError(this, "Select Logo");
            return false;
        } else if (shopNameET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Shop Name");
            return false;
        } else if (emailET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Email");
            return false;
        } else if (mobileNoET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Mobile");
            return false;
        } else if (websiteET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Website Link");
            return false;
        } else if (selectAddressMap.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Select Address on Map");
            return false;
        } else if (countryET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Select Country");
            return false;
        } else if (stateET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Select State");
            return false;
        } else if (cityET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Select City");
            return false;
        } else if (localityET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Locality");
            return false;
        } else if (subLocalityET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Sub-Locality");
            return false;
        } else if (currencyET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Select Currency");
            return false;
        } else if (bioET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Bio");
            return false;
        } else
            return true;
    }


    public static void start(Context context) {
        Intent intent = new Intent(context, ProfileActivity.class);
        context.startActivity(intent);
    }


}
