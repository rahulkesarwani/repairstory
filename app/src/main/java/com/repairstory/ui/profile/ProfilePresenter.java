package com.repairstory.ui.profile;

import com.repairstory.baseactivities.base.BasePresenter;
import com.repairstory.controller.IDataManager;
import com.repairstory.models.logins.LoginSignupRequestModel;
import com.repairstory.models.logins.LoginResponseModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import retrofit2.Response;

/**
 *
 * */
public class ProfilePresenter<V extends ProfileMvpView> extends BasePresenter<V> implements ProfileMvpPresenter<V> {


    public ProfilePresenter(IDataManager IDataManager) {
        super(IDataManager);
    }


    @Override
    public void hitProfileUpdateApi(MultipartBody request) {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitProfileUpdateApi(request)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {

                            getMvpView().onSuccess(commonResponse);
                        }, throwable -> {

                            getMvpView().onFailure(throwable);
                        })
        );
    }
}
