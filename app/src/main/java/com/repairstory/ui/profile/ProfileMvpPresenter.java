package com.repairstory.ui.profile;

import com.repairstory.baseactivities.base.MvpPresenter;
import com.repairstory.models.logins.LoginSignupRequestModel;

import okhttp3.MultipartBody;

/**
 * Created by rahul on 6/14/2018.
 */

public interface ProfileMvpPresenter<V extends ProfileMvpView>  extends MvpPresenter<V> {
    void hitProfileUpdateApi(MultipartBody request);
}