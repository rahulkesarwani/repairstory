package com.repairstory.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.repairstory.R;
import com.repairstory.baseactivities.LocationImagePickerBaseActivity;
import com.repairstory.ui.home.HomeActivity;
import com.repairstory.ui.login.LoginActivity;
import com.repairstory.utils.AppConstants;
import com.repairstory.utils.MySharedPreferences;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.logoIV)
    ImageView logoIV;

    private int SPLASH_DELAY = 2300;
//    private Animation anim;
    private MySharedPreferences mySharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        mySharedPreferences = new MySharedPreferences(this);
//        startAnimation();

        new Handler().postDelayed(() -> {
//            anim.cancel();
            handleLogin();
            finish();
        }, SPLASH_DELAY);
    }



    private void handleLogin() {
        String accesToken = mySharedPreferences.getString(AppConstants.ACCESS_TOKEN, "");
        if (!TextUtils.isEmpty(accesToken)) {
            startActivity(new Intent(this, HomeActivity.class));

        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
        finish();
    }

/*
    public void startAnimation() {
        anim = AnimationUtils.loadAnimation(this, R.anim.logo_animator);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationEnd(Animation arg0) {
                anim = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.logo_animator);
                anim.setAnimationListener(this);
                logoIV.startAnimation(anim);
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub

            }

        });
        logoIV.startAnimation(anim);
    }*/
}
