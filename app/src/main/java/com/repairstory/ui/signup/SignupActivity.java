package com.repairstory.ui.signup;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.repairstory.R;
import com.repairstory.baseactivities.BaseActivity;
import com.repairstory.models.logins.LoginResponseModel;
import com.repairstory.models.logins.LoginSignupRequestModel;
import com.repairstory.ui.home.HomeActivity;
import com.repairstory.ui.login.LoginActivity;
import com.repairstory.utils.AppConstants;
import com.repairstory.utils.Helper;
import com.repairstory.utils.MessagesUtils;
import com.repairstory.utils.customviews.CustomButton;
import com.repairstory.utils.customviews.CustomEditText;
import com.repairstory.utils.customviews.CustomTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;


public class SignupActivity extends BaseActivity implements SignupMvpView {


    @BindView(R.id.shopNameET)
    CustomEditText shopNameET;
    @BindView(R.id.userNameET)
    CustomEditText userNameET;
    @BindView(R.id.emailET)
    CustomEditText emailET;
    @BindView(R.id.passwordET)
    CustomEditText passwordET;
    @BindView(R.id.signupBtn)
    CustomButton signupBtn;
    @BindView(R.id.loginAccBtn)
    CustomTextView loginAccBtn;
    private String TAG = SignupActivity.class.getSimpleName();

    @Inject
    SignupMvpPresenter<SignupMvpView> mPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(SignupActivity.this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(SignupActivity.this);

        initUI();

    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_signup;
    }

    @Override
    protected void initUI() {
        initErrorViews(findViewById(R.id.error_views_layout));
        loginAccBtn.setText(Helper.fromHtml(getString(R.string.login_text)));
    }


    private void hitSignupApi() {

        if (checkBeforeApiHit()) {
            LoginSignupRequestModel requestModel = new LoginSignupRequestModel();
            requestModel.setUsername(userNameET.getText().toString().trim());
            requestModel.setPassword(passwordET.getText().toString().trim());
            requestModel.setEmail(emailET.getText().toString().trim());
            requestModel.setShopName(shopNameET.getText().toString().trim());
            mPresenter.hitSignupApi(requestModel);
        }
    }


    @Override
    public void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();

    }

    @Override
    protected void handleNoInternet() {

        hitSignupApi();
    }

    @Override
    protected void handleSlowInternet() {

        hitSignupApi();
    }

    @Override
    protected void handleServerError() {
        hitSignupApi();
    }


    @Override
    public void onSuccess(Response<LoginResponseModel> response) {

        hideProgressBar();


        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(SignupActivity.this, errorResponse);
        } else if (isResponseOK((short) response.code())) {

            if (response.body().getSuccess()) {

                mySharedPreferences.putString(AppConstants.ACCESS_TOKEN, response.body().getData().getAuthToken());
                mySharedPreferences.setObj(AppConstants.LOGIN_DATA, response.body().getData());

                startActivity(HomeActivity.class);
                finish();

            } else {
                showToast(response.body().getMessage());
            }
        }
    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    private boolean validateFields() {
        if (shopNameET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Shop Name");
            return false;
        } else if (userNameET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Username");
            return false;
        } else if (emailET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Email");
            return false;
        } else if (!validateEmail(emailET.getText().toString().trim())) {
            MessagesUtils.showToastError(this, "Enter Valid Email");
            return false;
        } else if (passwordET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Password");
            return false;
        } else if (passwordET.getText().toString().trim().length() < 8) {
            MessagesUtils.showToastError(this, "Password should be minimum 8 length");
            return false;
        } else {
            return true;
        }
    }

    @OnClick({R.id.signupBtn, R.id.loginAccBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.signupBtn:
                if (validateFields())
                    hitSignupApi();
                break;
            case R.id.loginAccBtn:
                startActivity(LoginActivity.class);
                break;
        }
    }
}
