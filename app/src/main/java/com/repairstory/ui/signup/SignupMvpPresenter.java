package com.repairstory.ui.signup;

import com.repairstory.baseactivities.base.MvpPresenter;
import com.repairstory.models.logins.LoginSignupRequestModel;

/**
 * Created by rahul on 6/14/2018.
 */

public interface SignupMvpPresenter<V extends SignupMvpView>  extends MvpPresenter<V> {
    void hitSignupApi(LoginSignupRequestModel loginRequestModel);
}