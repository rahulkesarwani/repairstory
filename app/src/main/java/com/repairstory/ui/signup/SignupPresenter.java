package com.repairstory.ui.signup;

import com.repairstory.baseactivities.base.BasePresenter;
import com.repairstory.controller.IDataManager;
import com.repairstory.models.logins.LoginSignupRequestModel;
import com.repairstory.models.logins.LoginResponseModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 *
 * */
public class SignupPresenter<V extends SignupMvpView> extends BasePresenter<V> implements SignupMvpPresenter<V> {


    public SignupPresenter(IDataManager IDataManager) {
        super(IDataManager);
    }



    @Override
    public void hitSignupApi(LoginSignupRequestModel loginRequestModel) {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitSignupApi(loginRequestModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {
                            getMvpView().onSuccess(commonResponse);
                        }, throwable -> {

                            getMvpView().onFailure(throwable);
                        })
        );
    }
}
