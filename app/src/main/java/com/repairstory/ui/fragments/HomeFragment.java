package com.repairstory.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.repairstory.R;
import com.repairstory.baseactivities.BaseFragment;
import com.repairstory.models.logins.Datum;
import com.repairstory.models.storycount.StoryCountModel;
import com.repairstory.ui.createstory.CreateEditStoryActivity;
import com.repairstory.ui.home.HomeActivity;
import com.repairstory.ui.login.LoginActivity;
import com.repairstory.ui.profile.ProfileActivity;
import com.repairstory.utils.AppConstants;
import com.repairstory.utils.CheckNullable;
import com.repairstory.utils.Helper;
import com.repairstory.utils.customviews.CustomTextView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    HomeActivity parentActivity;
    @BindView(R.id.userNameTV)
    CustomTextView userNameTV;
    @BindView(R.id.createStoryBtn)
    RelativeLayout createStoryBtn;
    @BindView(R.id.openStoryCountTV)
    CustomTextView openStoryCountTV;
    @BindView(R.id.openTxt)
    CustomTextView openTxt;
    @BindView(R.id.openStoryBtn)
    RelativeLayout openStoryBtn;
    @BindView(R.id.complettedStoryCountTV)
    CustomTextView complettedStoryCountTV;
    @BindView(R.id.completedTxt)
    CustomTextView completedTxt;
    @BindView(R.id.completedStoriesBtn)
    RelativeLayout completedStoriesBtn;
    @BindView(R.id.profileBtn)
    RelativeLayout profileBtn;
    @BindView(R.id.topLL)
    LinearLayout topLL;

    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeRefreshLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        parentActivity = (HomeActivity) getActivity();
        swipeRefreshLayout.setOnRefreshListener(this);

        setUsersCounts();
        hitCountStoriesCount();
        return view;
    }

    private void setUsersCounts() {

        Datum loginData = (Datum) mySharedPreferences.getObj(AppConstants.LOGIN_DATA, Datum.class);
        if (loginData != null) {
            userNameTV.setText("Hello, " + CheckNullable.checkNullable(loginData.getName()) + " !");
        }
    }


    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return 0;
    }

    @Override
    protected void handleNoInternet() {

    }

    @Override
    protected void handleSlowInternet() {

    }

    @Override
    protected void handleServerError() {

    }

    @OnClick({R.id.createStoryBtn, R.id.openStoryBtn, R.id.completedStoriesBtn, R.id.profileBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.createStoryBtn:
                if (Helper.isProfileCompleted(parentActivity.mySharedPreferences)) {
                    CreateEditStoryActivity.start(parentActivity, AppConstants.STORY_STEP.step1, 0, AppConstants.MODE_TYPE_CONSTANTS.CREATE);
                } else {
                    parentActivity.showAlertDialog("Please Complete Profile First!");
                }
                break;
            case R.id.openStoryBtn:

                parentActivity.loadOpenStoriesFragment(AppConstants.STORY_TYPE_CONSTANTS.OPEN);
                break;
            case R.id.completedStoriesBtn:
                StoriesListFragment completedStoriesListFragment = new StoriesListFragment();
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.STORY_TYPE, AppConstants.STORY_TYPE_CONSTANTS.COMPLETED);
                completedStoriesListFragment.setArguments(bundle);
                parentActivity.changeFragment(completedStoriesListFragment, AppConstants.COMPLETD_STORY_FRAGMENT);
                break;
            case R.id.profileBtn:
                ProfileActivity.start(parentActivity);
                break;
        }
    }

    private void hitCountStoriesCount() {

        if (mConnectionDetector.isInternetConnected()) {

            HashMap<String, String> map = new HashMap<>();
            map.put("status", "count");

            mCompositeDisposable.add(
                    commonApiInterface.hitCountTotalStoryApi(map)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {
                                onSuccess(commonResponse);
                            }, throwable -> {
                                onFailure(throwable);
                            })
            );
        }
    }


    public void onSuccess(Response<StoryCountModel> response) {
        hideRefresh();

        if (response.code() == 401) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }

        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (response.code() == 200) {
            complettedStoryCountTV.setText("" + CheckNullable.checkNullable(response.body().getData().getTotalCompleted()));
            openStoryCountTV.setText("" + CheckNullable.checkNullable(response.body().getData().getTotalOpen()));
        }

    }


    public void onFailure(Throwable error) {
        error.printStackTrace();
        hideRefresh();
    }

    private void hideRefresh() {
        if (swipeRefreshLayout != null)
            if (swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        hitCountStoriesCount();
    }
}
