package com.repairstory.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.repairstory.R;
import com.repairstory.adapters.recyclerview.StoriesListAdapter;
import com.repairstory.baseactivities.BaseFragment;
import com.repairstory.models.story.ListStoryModel;
import com.repairstory.models.story.StoryModel;
import com.repairstory.models.story.SuccessModel;
import com.repairstory.ui.createstory.CreateEditStoryActivity;
import com.repairstory.ui.login.LoginActivity;
import com.repairstory.utils.AppConstants;
import com.repairstory.utils.CheckNullable;
import com.repairstory.utils.EndlessRecyclerViewScrollListener;
import com.repairstory.utils.Helper;
import com.repairstory.utils.customviews.CustomTextView;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;


public class StoriesListFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.backIV)
    ImageView backIV;
    @BindView(R.id.toplayout)
    RelativeLayout toplayout;
    @BindView(R.id.storiesTypeTV)
    CustomTextView storiesTypeTV;
    @BindView(R.id.listRV)
    RecyclerView listRV;
    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private View mView;
    private ArrayList<StoryModel> mList = new ArrayList<>();
    private StoriesListAdapter storiesListAdapter;
    private String storyType;
    private int hitApiCounter;

    private int selectedPosition;
    private EndlessRecyclerViewScrollListener scrollListener;
    private int totalPage;
    private int currentPage = 1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_stories_list, container, false);
        ButterKnife.bind(this, mView);

        swipeRefreshLayout.setOnRefreshListener(this);
        storyType = getArguments().getString(AppConstants.STORY_TYPE);
        if (storyType.equalsIgnoreCase(AppConstants.STORY_TYPE_CONSTANTS.OPEN)) {
            storiesTypeTV.setText("Open Stories");
        } else {
            storiesTypeTV.setText("Completed Stories");
        }
        initErrorViews(mView.findViewById(R.id.error_views_layout));
        initList();
        return mView;
    }

    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return 0;
    }

    @Override
    protected void handleNoInternet() {
        checkAndHitApi();
    }

    @Override
    protected void handleSlowInternet() {
        checkAndHitApi();
    }

    @Override
    protected void handleServerError() {
        checkAndHitApi();
    }

    @Override
    public void onResume() {
        super.onResume();
        hitListApi();
    }

    private void checkAndHitApi() {
        if (hitApiCounter == 1) {
            hitListApi();
        } else if (hitApiCounter == 2) {
            hitDeleteStoryApi();
        }
    }

    private void initList() {
        storiesListAdapter = new StoriesListAdapter(getActivity(), mList, storyType, new StoriesListAdapter.RowClickListener() {
            @Override
            public void onRowClicked(int position) {

                selectedPosition = position;
                handleStoryStepNavigation();
            }

            @Override
            public void onDeleteClicked(int position) {
                selectedPosition = position;
                Helper.showConfirmationDialog(getActivity(), "Are you sure want to delete this story?", new Helper.DialogCLick() {
                    @Override
                    public void onYesClicked() {
                        hitDeleteStoryApi();
                    }

                    @Override
                    public void onNoClicked() {

                    }
                });


            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        listRV.setLayoutManager(linearLayoutManager);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (currentPage < totalPage) {
                    currentPage += 1;
                    listRV.post(new Runnable() {
                        @Override
                        public void run() {
                            mList.add(null);
                            storiesListAdapter.notifyItemInserted(storiesListAdapter.getItemCount() - 1);
                            hitListApi();
                        }
                    });
                }
            }
        };
        listRV.addOnScrollListener(scrollListener);
        listRV.setAdapter(storiesListAdapter);
    }


    private void hitListApi() {
        hitApiCounter = 1;
        if (checkBeforeApiHit(currentPage)) {

            HashMap<String, String> map = new HashMap<>();
            map.put("status", storyType);
            map.put("page", "" + currentPage);

            mCompositeDisposable.add(
                    commonApiInterface.hitStoriesListApi(map)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {
                                onSuccess(commonResponse);
                            }, throwable -> {
                                onFailure(throwable);
                            })
            );
        }
    }

    private void hitDeleteStoryApi() {
        hitApiCounter = 2;
        if (checkBeforeApiHit()) {
            mCompositeDisposable.add(
                    commonApiInterface.hitDeleteStoryApi(mList.get(selectedPosition).getId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {
                                onDeleteSuccess(commonResponse);
                            }, throwable -> {
                                onFailure(throwable);
                            })
            );
        }
    }


    public void onDeleteSuccess(Response<SuccessModel> response) {

        hideProgressBar();

        if (response.code() == 401) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }


        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (isResponseOK((short) response.code())) {
            Helper.showSuccessDialog(getActivity(), "Story Deleted Successfully!", new Helper.ConfirmClickListener() {
                @Override
                public void onConfirmClicked() {
                    mList.remove(selectedPosition);
                    storiesListAdapter.notifyItemRemoved(selectedPosition);
                }
            });
        }

    }


    public void onSuccess(Response<ListStoryModel> response) {
        hideRefresh();
        hideProgressBar();
        if (currentPage > 1) {
            mList.remove(mList.size() - 1);
            storiesListAdapter.notifyItemRemoved(mList.size());
        } else {
            mList.clear();
            storiesListAdapter.notifyDataSetChanged();
        }

        if (response.code() == 401) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }

        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (isResponseOK((short) response.code())) {


            mList.addAll(response.body().getData().getStories());

            storiesListAdapter.notifyDataSetChanged();
            if (response.body().getData().getLastPage() != null) {
                totalPage = response.body().getData().getLastPage();
            }
//            if (currentPage != 1) {
            scrollListener.setLoading();

            if (mList.size() > 0) {
                showNoDataFindView(false);
            } else {
                showNoDataFindView(true);
            }
        }

    }


    public void onFailure(Throwable error) {
        hideRefresh();
        handleFailure((Exception) error);
    }

    @Override
    public void onRefresh() {
        currentPage = 1;
        hitListApi();
    }

    private void hideRefresh() {
        if (swipeRefreshLayout != null)
            if (swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
    }

    private void handleStoryStepNavigation() {

        if (CheckNullable.checkNullable(mList.get(selectedPosition).getStep()).equalsIgnoreCase(AppConstants.STORY_STEP.step1)) {
            //move to step 2
            CreateEditStoryActivity.start(getActivity(), AppConstants.STORY_STEP.step2, mList.get(selectedPosition).getId(), AppConstants.MODE_TYPE_CONSTANTS.EDIT);
        } else if (CheckNullable.checkNullable(mList.get(selectedPosition).getStep()).equalsIgnoreCase(AppConstants.STORY_STEP.step2)) {
            //move to step 3
            CreateEditStoryActivity.start(getActivity(), AppConstants.STORY_STEP.step3, mList.get(selectedPosition).getId(), AppConstants.MODE_TYPE_CONSTANTS.EDIT);
        } else if (CheckNullable.checkNullable(mList.get(selectedPosition).getStep()).equalsIgnoreCase(AppConstants.STORY_STEP.step3)) {
            //move to testimonial
            CreateEditStoryActivity.start(getActivity(), AppConstants.STORY_STEP.testimonial, mList.get(selectedPosition).getId(), AppConstants.MODE_TYPE_CONSTANTS.EDIT);
        }
    }

    @OnClick(R.id.backIV)
    public void onViewClicked() {
        getActivity().onBackPressed();
    }
}
