package com.repairstory.ui.createstory;

import com.repairstory.baseactivities.base.MvpView;
import com.repairstory.models.logins.LoginResponseModel;
import com.repairstory.models.story.SuccessModel;
import com.repairstory.models.storydetail.StoryDetailModel;

import retrofit2.Response;


public interface CreateEditStoryMvpView extends MvpView {

    void onSuccess(Response<StoryDetailModel> response);

    void onFailure(Throwable error);

}
