package com.repairstory.ui.createstory.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.repairstory.MyApp;
import com.repairstory.R;
import com.repairstory.adapters.recyclerview.MediaListAdapter;
import com.repairstory.baseactivities.BaseFragment;
import com.repairstory.models.story.CreateUpdateStoryModel;
import com.repairstory.models.story.StoryModel;
import com.repairstory.models.story.SuccessModel;
import com.repairstory.ui.createstory.CreateEditStoryActivity;
import com.repairstory.ui.dialogs.CostDurationDialog;
import com.repairstory.ui.login.LoginActivity;
import com.repairstory.utils.AppConstants;
import com.repairstory.utils.CheckNullable;
import com.repairstory.utils.GridSpacingItemDecoration;
import com.repairstory.utils.Helper;
import com.repairstory.utils.MessagesUtils;
import com.repairstory.utils.customviews.CustomButton;
import com.repairstory.utils.customviews.CustomEditText;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;


public class Step3Fragment extends BaseFragment {

    @BindView(R.id.descriptionET)
    CustomEditText descriptionET;
    @BindView(R.id.addMediaBtn)
    RelativeLayout addMediaBtn;
    @BindView(R.id.textTitleTV)
    TextView textTitleTV;
    @BindView(R.id.mediaRV)
    RecyclerView mediaRV;
    @BindView(R.id.addTestimonialBtn)
    CustomButton addTestimonialBtn;
    @BindView(R.id.finishBtn)
    CustomButton finishBtn;
    private View mView;
    private MediaListAdapter mediaListAdapter;
    private ArrayList<String> mMediaList = new ArrayList<>();

    private CreateEditStoryActivity parentActivity;

    private boolean isTestimonialClicked;
    private String cost, duration;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_step3, container, false);
        ButterKnife.bind(this, mView);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        initErrorViews(mView.findViewById(R.id.error_views_layout));
        parentActivity = (CreateEditStoryActivity) getActivity();
        initList();
        return mView;
    }

    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return 0;
    }

    @Override
    protected void handleNoInternet() {
        hitUpdateStoryApi();
    }

    @Override
    protected void handleSlowInternet() {
        hitUpdateStoryApi();
    }

    @Override
    protected void handleServerError() {
        hitUpdateStoryApi();
    }

    @Override
    public void onResume() {
        super.onResume();
        //
    }

    private void initList() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        mediaRV.addItemDecoration(new GridSpacingItemDecoration(3, 10, true));
        mediaRV.setLayoutManager(gridLayoutManager);
        mediaListAdapter = new MediaListAdapter(getActivity(), mMediaList);
        mediaRV.setAdapter(mediaListAdapter);

    }


    @OnClick({R.id.addMediaBtn, R.id.addTestimonialBtn, R.id.finishBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addMediaBtn:
                if (mMediaList.size() <= 10) {
                    parentActivity.resultPath = "";
                    parentActivity.pickFromGallery();
                } else
                    MessagesUtils.showSnackbar(getContext(), "Can't exceed except 10 media");

                break;
            case R.id.addTestimonialBtn:
                isTestimonialClicked = true;
                // upload all media
                if (mMediaList.size() > 0)
                    parentActivity.uploadPresenter.hitUploadContent(MyApp.getInstance(), mMediaList, parentActivity.storyId, AppConstants.STORY_STEP.step3, mediaListAdapter.getDeletedServerMediaList());


                hitUpdateStoryApi();
                break;
            case R.id.finishBtn:

                isTestimonialClicked = false;
                showCostDurationDialog();
                break;
        }
    }


    private void showCostDurationDialog() {
        CostDurationDialog costDurationDialog = new CostDurationDialog(getActivity());
        costDurationDialog.setCostDurationListener(new CostDurationDialog.CostDurationListener() {
            @Override
            public void onDoneClicked(String cost, String duration) {

                // upload all media
                if (mMediaList.size() > 0)
                    parentActivity.uploadPresenter.hitUploadContent(MyApp.getInstance(), mMediaList, parentActivity.storyId, AppConstants.STORY_STEP.step3, mediaListAdapter.getDeletedServerMediaList());

                Step3Fragment.this.cost = cost;
                Step3Fragment.this.duration = duration;
                hitUpdateStoryApi();
            }
        });
        costDurationDialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == parentActivity.SELECT_PICTURE && resultCode == parentActivity.RESULT_OK) {
            if (!TextUtils.isEmpty(parentActivity.resultPath)) {
                mMediaList.add(parentActivity.resultPath);
                mediaListAdapter.notifyItemChanged(mMediaList.size() - 1);
            }
        }
    }

    private void hitUpdateStoryApi() {
        enableDisableButton(false);
        if (checkBeforeApiHit()) {
            CreateUpdateStoryModel createStoryModel = new CreateUpdateStoryModel();
            createStoryModel.setFinalRepairUpdate(descriptionET.getText().toString().trim());

            if (isTestimonialClicked) {
                createStoryModel.setStatus(AppConstants.STORY_TYPE_CONSTANTS.OPEN);
            } else {
                createStoryModel.setStatus(AppConstants.STORY_TYPE_CONSTANTS.COMPLETED);
                createStoryModel.setDuration(duration);
                createStoryModel.setCost(cost);
            }
            createStoryModel.setStep(AppConstants.STORY_STEP.step3);

            mCompositeDisposable.add(
                    commonApiInterface.hitUpdateStoryApi(parentActivity.storyId, createStoryModel)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {
                                onSuccess(commonResponse);
                            }, throwable -> {
                                onFailure(throwable);
                            })
            );
        }
    }

    public void onSuccess(Response<SuccessModel> response) {
        enableDisableButton(true);
        hideProgressBar();

        if (response.code() == 401) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }

        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (isResponseOK((short) response.code())) {


            if (isTestimonialClicked) {
                parentActivity.viewPager.setCurrentItem(3);
//                CreateEditStoryActivity.start(getActivity(), AppConstants.STORY_STEP.testimonial, parentActivity.storyId);
            } else {
                Helper.showSuccessDialog(getActivity(), "Story Updated Successfully!", new Helper.ConfirmClickListener() {
                    @Override
                    public void onConfirmClicked() {
                        EventBus.getDefault().postSticky(AppConstants.STORY_TYPE_CONSTANTS.COMPLETED);
                        getActivity().finish();
                    }
                });
            }
        }

    }

    public void onFailure(Throwable error) {
        enableDisableButton(true);
        handleFailure((Exception) error);
    }


    private void updateUI() {
        if (parentActivity.responseData != null) {

            descriptionET.setText(CheckNullable.checkNullable(parentActivity.responseData.getFinalRepairUpdate()));

            mMediaList.clear();
            mMediaList.addAll(parentActivity.responseData.getMedia().getPics().getStep3());
            mMediaList.addAll(parentActivity.responseData.getMedia().getVideos().getStep3());
            mediaListAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onDestroyView() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroyView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(StoryModel updatedStoryModel) {
        updateUI();
    }

    private void enableDisableButton(boolean check){
        finishBtn.setEnabled(check);
    }
}
