package com.repairstory.ui.createstory;

import com.repairstory.baseactivities.base.MvpPresenter;

/**
 * Created by rahul on 6/14/2018.
 */

public interface CreateEditStoryMvpPresenter<V extends CreateEditStoryMvpView>  extends MvpPresenter<V> {
    void getStoryDetail(int id);
}