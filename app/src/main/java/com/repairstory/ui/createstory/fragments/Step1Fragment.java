package com.repairstory.ui.createstory.fragments;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.repairstory.MyApp;
import com.repairstory.R;
import com.repairstory.adapters.recyclerview.MediaListAdapter;
import com.repairstory.baseactivities.BaseFragment;
import com.repairstory.models.category.Category;
import com.repairstory.models.category.CategoryResponseModel;
import com.repairstory.models.story.CreateUpdateStoryModel;
import com.repairstory.models.story.StoryModel;
import com.repairstory.models.story.SuccessModel;
import com.repairstory.ui.createstory.CreateEditStoryActivity;
import com.repairstory.ui.login.LoginActivity;
import com.repairstory.utils.AppConstants;
import com.repairstory.utils.CheckNullable;
import com.repairstory.utils.GridSpacingItemDecoration;
import com.repairstory.utils.Helper;
import com.repairstory.utils.MessagesUtils;
import com.repairstory.utils.customviews.CustomButton;
import com.repairstory.utils.customviews.CustomEditText;
import com.repairstory.utils.customviews.CustomTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;


public class Step1Fragment extends BaseFragment {


    @BindView(R.id.storyTypeSpinner)
    AppCompatSpinner storyTypeSpinner;
    @BindView(R.id.deviceDetailsET)
    CustomEditText deviceDetailsET;
    @BindView(R.id.descriptionET)
    CustomEditText descriptionET;
    @BindView(R.id.addMediaBtn)
    RelativeLayout addMediaBtn;
    @BindView(R.id.mediaRV)
    RecyclerView mediaRV;
    @BindView(R.id.createStoryBtn)
    CustomButton createStoryBtn;
    private View mView;

    private CreateEditStoryActivity parentActivity;

    private ArrayList<String> mMediaList = new ArrayList<>();
    private MediaListAdapter mediaListAdapter;
    private int hitApiCounter;
    private List<Category> categoryList = new ArrayList<>();
    private final int REQ_CODE_SPEECH_INPUT = 500;
    private String[] categoryStringList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_step1, container, false);
        ButterKnife.bind(this, mView);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        initErrorViews(mView.findViewById(R.id.error_views_layout));
        parentActivity = (CreateEditStoryActivity) getActivity();

        if (parentActivity.currentMode.equalsIgnoreCase(AppConstants.MODE_TYPE_CONSTANTS.EDIT)) {
            createStoryBtn.setText("Update Story");

        } else {
            createStoryBtn.setText("Create Story");
        }

        initList();
        getCategoryApi();
        return mView;
    }

    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return 0;
    }

    @Override
    protected void handleNoInternet() {
        checkAndHitApi();
    }

    @Override
    protected void handleSlowInternet() {
        checkAndHitApi();
    }

    @Override
    protected void handleServerError() {
        checkAndHitApi();
    }

    @Override
    public void onResume() {
        super.onResume();
        // hitCreateNewStoryApi();
    }

    private void checkAndHitApi() {
        if (hitApiCounter == 1) {
            getCategoryApi();
        } else if (hitApiCounter == 2) {
            hitCreateNewStoryApi();
        }
    }


    private void initList() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        mediaRV.addItemDecoration(new GridSpacingItemDecoration(3, 10, true));
        mediaRV.setLayoutManager(gridLayoutManager);
        mediaListAdapter = new MediaListAdapter(getActivity(), mMediaList);
        mediaRV.setAdapter(mediaListAdapter);

    }


    private void hitCreateNewStoryApi() {
        hitApiCounter = 2;
        if (checkBeforeApiHit()) {

            CreateUpdateStoryModel createStoryModel = new CreateUpdateStoryModel();
            createStoryModel.setCategoryId(categoryList.get(storyTypeSpinner.getSelectedItemPosition()).getId());
            createStoryModel.setDescription(descriptionET.getText().toString().trim());
            createStoryModel.setDeviceDetails(deviceDetailsET.getText().toString().trim());
            createStoryModel.setStatus(AppConstants.STORY_TYPE_CONSTANTS.OPEN);
            createStoryModel.setStep(AppConstants.STORY_STEP.step1);


            if (parentActivity.currentMode.equalsIgnoreCase(AppConstants.MODE_TYPE_CONSTANTS.CREATE)) {
                mCompositeDisposable.add(
                        commonApiInterface.hitCreateNewStoryApi(createStoryModel)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(commonResponse -> {
                                    onSuccess(commonResponse);
                                }, throwable -> {
                                    onFailure(throwable);
                                })
                );
            } else {
                mCompositeDisposable.add(
                        commonApiInterface.hitUpdateStoryApi(parentActivity.storyId, createStoryModel)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(commonResponse -> {
                                    onSuccess(commonResponse);
                                }, throwable -> {
                                    onFailure(throwable);
                                })
                );
            }
        }
    }


    private void enableDisableButton(boolean check){
        createStoryBtn.setEnabled(check);
    }

    public void onSuccess(Response<SuccessModel> response) {

        enableDisableButton(true);
        hideProgressBar();

        if (response.code() == 401) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }

        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (isResponseOK((short) response.code())) {

            // upload all media

            if (parentActivity.currentMode.equalsIgnoreCase(AppConstants.MODE_TYPE_CONSTANTS.CREATE)) {

                Helper.showSuccessDialog(getActivity(), "Story Created Successfully!", new Helper.ConfirmClickListener() {
                    @Override
                    public void onConfirmClicked() {
                        if (mMediaList.size() > 0) {
                            parentActivity.uploadPresenter.hitUploadContent(MyApp.getInstance(), mMediaList, response.body().getData().getStoryId(), AppConstants.STORY_STEP.step1, mediaListAdapter.getDeletedServerMediaList());
                        }
                        EventBus.getDefault().postSticky(AppConstants.STORY_TYPE_CONSTANTS.OPEN);
                        getActivity().finish();
                    }
                });

            } else {
                parentActivity.uploadPresenter.hitUploadContent(MyApp.getInstance(), mMediaList, parentActivity.storyId, AppConstants.STORY_STEP.step1, mediaListAdapter.getDeletedServerMediaList());
                parentActivity.viewPager.setCurrentItem(1);
            }
        }
    }

    public void onFailure(Throwable error) {
        enableDisableButton(true);
        handleFailure((Exception) error);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == parentActivity.SELECT_PICTURE && resultCode == parentActivity.RESULT_OK) {
            if (!TextUtils.isEmpty(parentActivity.resultPath)) {
                mMediaList.add(parentActivity.resultPath);
                mediaListAdapter.notifyItemChanged(mMediaList.size() - 1);

            }
        } else if (resultCode == parentActivity.RESULT_OK) {

            if (data == null)
                return;
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            deviceDetailsET.setText(result.get(0));


        }
    }


    @OnClick({R.id.addMediaBtn, R.id.createStoryBtn, R.id.speechIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addMediaBtn:
                if (mMediaList.size() <= 10) {
                    parentActivity.resultPath = "";
                    parentActivity.pickFromGallery();
                } else
                    MessagesUtils.showSnackbar(getContext(), "Can't exceed except 10 media");
                break;
            case R.id.createStoryBtn:
                if (validateFields()) {
                    enableDisableButton(false);
                    hitCreateNewStoryApi();
                }
                break;
            case R.id.speechIV:
                askSpeechInput();
                break;
        }
    }

    private boolean validateFields() {
        if (deviceDetailsET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(getContext(), "Enter Device Details");
            return false;
        } else {
            return true;
        }
    }


    private void getCategoryApi() {
        hitApiCounter = 1;
        if (checkBeforeApiHit()) {

            mCompositeDisposable.add(
                    commonApiInterface.getCategoriesApi()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {
                                onCategorySuccess(commonResponse);
                            }, throwable -> {
                                onFailure(throwable);
                            })
            );
        }
    }

    public void onCategorySuccess(Response<CategoryResponseModel> response) {

        hideProgressBar();

        if (response.code() == 401) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }

        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (isResponseOK((short) response.code())) {

            categoryList.addAll(response.body().getData());
            categoryStringList = new String[categoryList.size()];
            for (int i = 0; i < categoryList.size(); i++) {
                categoryStringList[i] = categoryList.get(i).getName();
            }

            ArrayAdapter<String> eventsAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categoryStringList);
            eventsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            storyTypeSpinner.setAdapter(eventsAdapter);
        }
    }


    private void askSpeechInput() {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Hi Please Speak Device Details");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {

        }
    }


    private void updateUI() {
        if (parentActivity.responseData != null) {

            for (int i = 0; i < categoryList.size(); i++) {

                if (CheckNullable.checkNullable(parentActivity.responseData.getCategoryId()).equals(categoryList.get(i).getId())) {
                    storyTypeSpinner.setSelection(i);
                    break;
                }

            }

            deviceDetailsET.setText(CheckNullable.checkNullable(parentActivity.responseData.getDeviceDetails()));
            descriptionET.setText(CheckNullable.checkNullable(parentActivity.responseData.getDescription()));

            mMediaList.clear();
            mMediaList.addAll(parentActivity.responseData.getMedia().getPics().getStep1());
            mMediaList.addAll(parentActivity.responseData.getMedia().getVideos().getStep1());
            mediaListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroyView() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroyView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(StoryModel updatedStoryModel) {
        updateUI();
    }


}
