package com.repairstory.ui.createstory;

import com.repairstory.baseactivities.base.BasePresenter;
import com.repairstory.controller.IDataManager;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 *
 */
public class CreateEditStoryPresenter<V extends CreateEditStoryMvpView> extends BasePresenter<V> implements CreateEditStoryMvpPresenter<V> {


    public CreateEditStoryPresenter(IDataManager IDataManager) {
        super(IDataManager);
    }

    @Override
    public void getStoryDetail(int id) {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitGetStoryApi(id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {
                            getMvpView().onSuccess(commonResponse);
                        }, throwable -> {

                            getMvpView().onFailure(throwable);
                        })
        );

    }
}
