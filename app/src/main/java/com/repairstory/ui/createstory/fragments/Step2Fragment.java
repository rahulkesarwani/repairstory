package com.repairstory.ui.createstory.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.repairstory.MyApp;
import com.repairstory.R;
import com.repairstory.adapters.recyclerview.MediaListAdapter;
import com.repairstory.baseactivities.BaseFragment;
import com.repairstory.models.story.CreateUpdateStoryModel;
import com.repairstory.models.story.StoryModel;
import com.repairstory.models.story.SuccessModel;
import com.repairstory.ui.createstory.CreateEditStoryActivity;
import com.repairstory.ui.login.LoginActivity;
import com.repairstory.utils.AppConstants;
import com.repairstory.utils.CheckNullable;
import com.repairstory.utils.GridSpacingItemDecoration;
import com.repairstory.utils.Helper;
import com.repairstory.utils.MessagesUtils;
import com.repairstory.utils.customviews.CustomButton;
import com.repairstory.utils.customviews.CustomEditText;
import com.repairstory.utils.customviews.CustomTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;


public class Step2Fragment extends BaseFragment {


    @BindView(R.id.descriptionET)
    CustomEditText descriptionET;
    @BindView(R.id.addMediaBtn)
    RelativeLayout addMediaBtn;
    @BindView(R.id.textTitleTV)
    TextView textTitleTV;
    @BindView(R.id.mediaRV)
    RecyclerView mediaRV;
    @BindView(R.id.createStoryBtn)
    CustomButton createStoryBtn;
    @BindView(R.id.skipStepBtn)
    CustomTextView skipStepBtn;
    @BindView(R.id.bottomLL)
    LinearLayout bottomLL;
    private View mView;
    private MediaListAdapter mediaListAdapter;
    private ArrayList<String> mMediaList = new ArrayList<>();

    private CreateEditStoryActivity parentActivity;
    private boolean isSkipClicked = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_step2, container, false);
        ButterKnife.bind(this, mView);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        initErrorViews(mView.findViewById(R.id.error_views_layout));
        parentActivity = (CreateEditStoryActivity) getActivity();
        initList();
        return mView;
    }

    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return 0;
    }

    @Override
    protected void handleNoInternet() {
        hitUpdateStoryApi();
    }

    @Override
    protected void handleSlowInternet() {
        hitUpdateStoryApi();
    }

    @Override
    protected void handleServerError() {
        hitUpdateStoryApi();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void initList() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        mediaRV.addItemDecoration(new GridSpacingItemDecoration(3, 10, true));
        mediaRV.setLayoutManager(gridLayoutManager);
        mediaListAdapter = new MediaListAdapter(getActivity(), mMediaList);
        mediaRV.setAdapter(mediaListAdapter);

    }


    @OnClick({R.id.addMediaBtn, R.id.skipStepBtn, R.id.createStoryBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addMediaBtn:
                if (mMediaList.size() <= 10) {
                    parentActivity.resultPath = "";
                    parentActivity.pickFromGallery();
                } else
                    MessagesUtils.showSnackbar(getContext(), "Can't exceed except 10 media");
                break;
            case R.id.skipStepBtn:
                isSkipClicked = true;
                if (validateFields()) {
                    hitUpdateStoryApi();
                }
                break;
            case R.id.createStoryBtn:
                isSkipClicked = false;
                enableDisableButton(false);
                if (validateFields()) {
                    // upload all media
                    if (mMediaList.size() > 0)
                        parentActivity.uploadPresenter.hitUploadContent(MyApp.getInstance(), mMediaList, parentActivity.storyId, AppConstants.STORY_STEP.step2, mediaListAdapter.getDeletedServerMediaList());

                    hitUpdateStoryApi();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == parentActivity.SELECT_PICTURE && resultCode == parentActivity.RESULT_OK) {
            if (!TextUtils.isEmpty(parentActivity.resultPath)) {
                mMediaList.add(parentActivity.resultPath);
                mediaListAdapter.notifyItemChanged(mMediaList.size() - 1);
            }
        }
    }

    private void hitUpdateStoryApi() {
        if (checkBeforeApiHit()) {
            CreateUpdateStoryModel createStoryModel = new CreateUpdateStoryModel();
            createStoryModel.setProgressDetails(descriptionET.getText().toString().trim());

            createStoryModel.setStatus(AppConstants.STORY_TYPE_CONSTANTS.OPEN);
            createStoryModel.setStep(AppConstants.STORY_STEP.step2);

            mCompositeDisposable.add(
                    commonApiInterface.hitUpdateStoryApi(parentActivity.storyId, createStoryModel)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {
                                onSuccess(commonResponse);
                            }, throwable -> {
                                onFailure(throwable);
                            })
            );
        }
    }

    public void onSuccess(Response<SuccessModel> response) {
        enableDisableButton(true);
        hideProgressBar();

        if (response.code() == 401) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }

        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (isResponseOK((short) response.code())) {
            Helper.showSuccessDialog(getActivity(), "Story Updated Successfully!", new Helper.ConfirmClickListener() {
                @Override
                public void onConfirmClicked() {
                    parentActivity.viewPager.setCurrentItem(2);
                }
            });
        }

    }

    public void onFailure(Throwable error) {
        enableDisableButton(true);
        handleFailure((Exception) error);
    }

    private boolean validateFields() {
        return true;
    }


    private void updateUI() {
        if (parentActivity.responseData != null) {

            descriptionET.setText(CheckNullable.checkNullable(parentActivity.responseData.getProgressDetails()));

            mMediaList.clear();
            mMediaList.addAll(parentActivity.responseData.getMedia().getPics().getStep2());
            mMediaList.addAll(parentActivity.responseData.getMedia().getVideos().getStep2());
            mediaListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroyView() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroyView();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(StoryModel updatedStoryModel) {
        updateUI();
    }

    private void enableDisableButton(boolean check){
        createStoryBtn.setEnabled(check);
    }
}
