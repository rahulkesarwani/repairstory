package com.repairstory.ui.createstory;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.repairstory.R;
import com.repairstory.adapters.viewpageradapters.HomePagerAdapter;
import com.repairstory.baseactivities.LocationImagePickerBaseActivity;
import com.repairstory.models.story.StoryModel;
import com.repairstory.models.storydetail.StoryDetailModel;
import com.repairstory.ui.createstory.fragments.AddTestimonialFragment;
import com.repairstory.ui.createstory.fragments.Step1Fragment;
import com.repairstory.ui.createstory.fragments.Step2Fragment;
import com.repairstory.ui.createstory.fragments.Step3Fragment;
import com.repairstory.ui.login.LoginActivity;
import com.repairstory.uploadfile.RepeatUploadPresenter;
import com.repairstory.utils.AppConstants;
import com.repairstory.utils.Helper;
import com.repairstory.utils.customviews.CustomTextView;
import com.repairstory.utils.customviews.CustomViewPager;
import com.shuhart.stepview.StepView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;


public class CreateEditStoryActivity extends LocationImagePickerBaseActivity implements CreateEditStoryMvpView {


    @BindView(R.id.backIV)
    ImageView backIV;
    @BindView(R.id.topLayout)
    RelativeLayout topLayout;
    @BindView(R.id.createStoryTxt)
    CustomTextView createStoryTxt;
    @BindView(R.id.stepView)
    StepView stepView;
    @BindView(R.id.viewPager)
    public CustomViewPager viewPager;
    private String TAG = CreateEditStoryActivity.class.getSimpleName();

    public String currentStep;
    @Inject
    CreateEditStoryMvpPresenter<CreateEditStoryMvpView> mPresenter;

    private HomePagerAdapter homePagerAdapter;
    public RepeatUploadPresenter uploadPresenter;

    public Integer storyId;
    private int currentVisibleFragment = 0;
    private Fragment step1Fragment, step2Fragment, step3Fragment, testimonialFragment;
    public String currentMode;
    public StoryModel responseData;
    private boolean showProgressBar = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(CreateEditStoryActivity.this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(CreateEditStoryActivity.this);

        currentStep = getIntent().getExtras().getString(AppConstants.CURRENT_STEP);
        currentMode = getIntent().getExtras().getString(AppConstants.CURRENT_MODE);

        Log.d(TAG, "onCreate: " + currentMode);
        if (!currentStep.equalsIgnoreCase(AppConstants.STORY_STEP.step1)) {
            storyId = getIntent().getExtras().getInt(AppConstants.STORY_ID);
        }

        uploadPresenter = new RepeatUploadPresenter(this);
        initUI();

    }

    @Override
    protected void gotCurrentLocation() {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_create_story;
    }

    @Override
    protected void initUI() {

        initErrorViews(findViewById(R.id.error_views_layout));

        if (currentMode.equalsIgnoreCase(AppConstants.MODE_TYPE_CONSTANTS.EDIT)) {
            createStoryTxt.setText("Update story");
            showProgressBar = true;
            hitStoryDetailApi();
        } else {
            createStoryTxt.setText("Create a new story");

        }
        initViewPager();
    }


    private void hitStoryDetailApi() {
        if (mConnectionDetector.isInternetConnected()) {
            if (showProgressBar)
                showLoading();
            mPresenter.getStoryDetail(storyId);
        }
    }


    @Override
    public void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();

    }

    @Override
    protected void handleNoInternet() {

        hitStoryDetailApi();
    }

    @Override
    protected void handleSlowInternet() {

        hitStoryDetailApi();
    }

    @Override
    protected void handleServerError() {
        hitStoryDetailApi();
    }


    @Override
    public void onSuccess(Response<StoryDetailModel> response) {
        hideProgressBar();

        if (response.code() == 401) {
            startActivity(new Intent(this, LoginActivity.class));
            finishAffinity();
            return;
        }


        if (!TextUtils.isEmpty(Helper.hasErrorBody(response))) {
            Helper.handleErrorBody(CreateEditStoryActivity.this, Helper.hasErrorBody(response));
        } else if (isResponseOK((short) response.code())) {

            this.responseData = response.body().getData();
            EventBus.getDefault().postSticky(response.body().getData());
        }
    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }


    private void initViewPager() {
        homePagerAdapter = new HomePagerAdapter(getSupportFragmentManager());

        step1Fragment = new Step1Fragment();
        step2Fragment = new Step2Fragment();
        step3Fragment = new Step3Fragment();
        testimonialFragment = new AddTestimonialFragment();


        homePagerAdapter.addFragment(step1Fragment, "");
        homePagerAdapter.addFragment(step2Fragment, "");
        homePagerAdapter.addFragment(step3Fragment, "");
        homePagerAdapter.addFragment(testimonialFragment, "");


        viewPager.disableScroll(true);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setAdapter(homePagerAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
                currentVisibleFragment = i;
//                updateSteps(currentVisibleFragment);
            }

            @Override
            public void onPageSelected(int i) {

                currentVisibleFragment = i;
//                updateSteps(currentVisibleFragment);
                stepView.go(i, false);
                Log.d(TAG, "onPageSelected: " + i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        if (currentStep.equalsIgnoreCase(AppConstants.STORY_STEP.step1)) {

        } else if (currentStep.equalsIgnoreCase(AppConstants.STORY_STEP.step2)) {
            updateSteps(1);
        } else if (currentStep.equalsIgnoreCase(AppConstants.STORY_STEP.step3)) {
            updateSteps(2);
        } else if (currentStep.equalsIgnoreCase(AppConstants.STORY_STEP.testimonial)) {
            updateSteps(3);
        }


        stepView.setOnStepClickListener(new StepView.OnStepClickListener() {
            @Override
            public void onStepClick(int step) {
                Log.d(TAG, "onStepClick: " + step);
                // if step1 so don't navigate in any steps
                if (!(currentStep.equalsIgnoreCase(AppConstants.STORY_STEP.step1)))
                    updateSteps(step);

            }
        });


    }

    public static void start(Context context, String step, int storyId, String currentMode) {
        Intent intent = new Intent(context, CreateEditStoryActivity.class);
        intent.putExtra(AppConstants.CURRENT_STEP, step);
        intent.putExtra(AppConstants.STORY_ID, storyId);
        intent.putExtra(AppConstants.CURRENT_MODE, currentMode);
        context.startActivity(intent);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
//            fragment.onActivityResult(requestCode, resultCode, data);
//        }

        switch (currentVisibleFragment) {
            case 0:
                step1Fragment.onActivityResult(requestCode, resultCode, data);
                break;
            case 1:
                step2Fragment.onActivityResult(requestCode, resultCode, data);
                break;
            case 2:
                step3Fragment.onActivityResult(requestCode, resultCode, data);
                break;
            case 3:
                testimonialFragment.onActivityResult(requestCode, resultCode, data);
                break;

        }
    }

    public void updateSteps(int stepNo) {
        stepView.go(stepNo, false);
        viewPager.setCurrentItem(stepNo);
    }


    @Override
    protected void onStop() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Boolean mediaUploadCompleted) {
        if (mediaUploadCompleted) {
            showProgressBar = false;
            hitStoryDetailApi();
        }
    }

    @OnClick(R.id.backIV)
    public void onViewClicked() {
        finish();
    }
}
