package com.repairstory.ui.home;

import android.os.Bundle;
import android.view.MenuItem;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.repairstory.R;
import com.repairstory.baseactivities.BaseActivity;
import com.repairstory.models.logins.LoginSignupRequestModel;
import com.repairstory.models.logins.LoginResponseModel;
import com.repairstory.ui.createstory.CreateEditStoryActivity;
import com.repairstory.ui.fragments.HomeFragment;
import com.repairstory.ui.fragments.StoriesListFragment;
import com.repairstory.ui.login.LoginActivity;
import com.repairstory.ui.profile.ProfileActivity;
import com.repairstory.utils.AppConstants;
import com.repairstory.utils.Helper;
import com.google.android.material.navigation.NavigationView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import retrofit2.Response;


public class HomeActivity extends BaseActivity implements HomeMvpView, NavigationView.OnNavigationItemSelectedListener {

    private String TAG = HomeActivity.class.getSimpleName();

    @Inject
    HomeMvpPresenter<HomeMvpView> mPresenter;
    private FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(HomeActivity.this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(HomeActivity.this);

        initUI();
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_home;
    }

    @Override
    protected void onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }

    @Override
    protected void initUI() {
//        initErrorViews(findViewById(R.id.error_views_layout));
        loadHomeFragment();
    }

    private void hitLoginApi() {

        if (checkBeforeApiHit()) {
            LoginSignupRequestModel loginRequestModel = new LoginSignupRequestModel();
            mPresenter.hitLogin(loginRequestModel);
        }
    }


    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        mPresenter.onDetach();
        super.onDestroy();

    }

    @Override
    protected void handleNoInternet() {

        hitLoginApi();
    }

    @Override
    protected void handleSlowInternet() {

        hitLoginApi();
    }

    @Override
    protected void handleServerError() {
        hitLoginApi();
    }

    @Override
    public void onSuccess(Response<LoginResponseModel> response) {
        if (isResponseOK((short) response.code())) {


        }

    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    public void loadOpenStoriesFragment(String storyType) {
        StoriesListFragment openStoriesListFragment = new StoriesListFragment();
        Bundle openbundle = new Bundle();
        openbundle.putString(AppConstants.STORY_TYPE, storyType);
        openStoriesListFragment.setArguments(openbundle);
        changeFragment(openStoriesListFragment, AppConstants.OPEN_STORY_FRAGMENT);
    }


    private void loadHomeFragment() {
        HomeFragment homeFragment = new HomeFragment();
        changeFragment(homeFragment, AppConstants.HOME_FRAGMENT);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {


        switch (menuItem.getItemId()) {

            case R.id.nav_createNewStory:
                if (Helper.isProfileCompleted(mySharedPreferences)) {
                    CreateEditStoryActivity.start(this, AppConstants.STORY_STEP.step1, 0, AppConstants.MODE_TYPE_CONSTANTS.CREATE);
                } else {
                    showAlertDialog("Please Complete Profile First!");
                }
                break;
            case R.id.nav_openStory:
                loadOpenStoriesFragment(AppConstants.STORY_TYPE_CONSTANTS.OPEN);
                break;
            case R.id.nav_completedStory:

                StoriesListFragment completedStoriesListFragment = new StoriesListFragment();
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.STORY_TYPE, AppConstants.STORY_TYPE_CONSTANTS.COMPLETED);
                completedStoriesListFragment.setArguments(bundle);
                changeFragment(completedStoriesListFragment, AppConstants.COMPLETD_STORY_FRAGMENT);

                break;
            case R.id.nav_Profile:
                ProfileActivity.start(this);
                break;
            case R.id.nav_Settings:

                break;
            case R.id.nav_logout:
                mySharedPreferences.setObj(AppConstants.LOGIN_DATA, null);
                mySharedPreferences.putString(AppConstants.ACCESS_TOKEN, "");

                startActivity(LoginActivity.class);
                finishAffinity();
                break;
            case R.id.nav_home:
                loadHomeFragment();
                break;

        }
        return true;
    }


    public void changeFragment(Fragment fragment, String tag) {
        try {
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.nav_host_fragment, fragment).addToBackStack(tag).commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


    @Override
    public void onBackPressed() {
        if (getVisibleFragment() != null && getVisibleFragment() instanceof HomeFragment) {
            finish();
        } else if (fragmentManager.getBackStackEntryCount() > 1) {
            super.onBackPressed();
        }
    }


    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String fragmentType) {

        if (fragmentType.equalsIgnoreCase(AppConstants.STORY_TYPE_CONSTANTS.OPEN)) {
            loadOpenStoriesFragment(AppConstants.STORY_TYPE_CONSTANTS.OPEN);

        } else if (fragmentType.equalsIgnoreCase(AppConstants.STORY_TYPE_CONSTANTS.COMPLETED)) {
            loadOpenStoriesFragment(AppConstants.STORY_TYPE_CONSTANTS.COMPLETED);
        }
    }
}
