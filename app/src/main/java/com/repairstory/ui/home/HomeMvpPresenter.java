package com.repairstory.ui.home;

import com.repairstory.baseactivities.base.MvpPresenter;
import com.repairstory.models.logins.LoginSignupRequestModel;

/**
 * Created by rahul on 6/14/2018.
 */

public interface HomeMvpPresenter<V extends HomeMvpView>  extends MvpPresenter<V> {
    void hitLogin(LoginSignupRequestModel loginRequestModel);
}