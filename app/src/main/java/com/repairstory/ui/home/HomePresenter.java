package com.repairstory.ui.home;

import com.repairstory.baseactivities.base.BasePresenter;
import com.repairstory.controller.IDataManager;
import com.repairstory.models.logins.LoginSignupRequestModel;
import com.repairstory.models.logins.LoginResponseModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 *
 * */
public class HomePresenter<V extends HomeMvpView> extends BasePresenter<V> implements HomeMvpPresenter<V> {


    public HomePresenter(IDataManager IDataManager) {
        super(IDataManager);
    }

    @Override
    public void hitLogin(LoginSignupRequestModel loginRequestModel) {

        getIDataManager().getCompositeDisposable().add(getIDataManager().getApiInterface().hitLoginApi(loginRequestModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));

    }


    private void handleResponse(Response<LoginResponseModel> value) {
        getMvpView().onSuccess(value);
    }

    private void handleError(Throwable error) {

        getMvpView().onFailure(error);
    }

}
