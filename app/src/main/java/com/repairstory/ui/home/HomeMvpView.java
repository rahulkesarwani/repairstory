package com.repairstory.ui.home;

import com.repairstory.baseactivities.base.MvpView;
import com.repairstory.models.logins.LoginResponseModel;

import retrofit2.Response;


public interface HomeMvpView extends MvpView {

    void onSuccess(Response<LoginResponseModel> response);

    void onFailure(Throwable error);

}
