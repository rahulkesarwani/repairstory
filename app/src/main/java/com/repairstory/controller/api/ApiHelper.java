package com.repairstory.controller.api;

import android.content.Context;

import com.repairstory.apis.ApiInterface;
import com.repairstory.di.annotations.ApplicationContext;
import com.repairstory.utils.ApiUtility;
import com.repairstory.utils.ConnectionDetector;
import com.repairstory.utils.MySharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by ilkay on 01/07/2017.
 */

@Singleton
public class ApiHelper implements IApiHelper {

    private CompositeDisposable mCompositeDisposable;
    private ApiInterface apiInterface;
    private ConnectionDetector mConnectionDetector;
    private MySharedPreferences mySharedPreferences;

    @Inject
    public ApiHelper(@ApplicationContext Context context)
    {
        mCompositeDisposable = new CompositeDisposable();
        apiInterface = ApiUtility.getInstance().getRetrofit().create(ApiInterface.class);
        mConnectionDetector = new ConnectionDetector(context);
        mySharedPreferences=new MySharedPreferences(context);
    }


    @Override
    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    @Override
    public ApiInterface getApiInterface() {
        return apiInterface;
    }

    @Override
    public boolean isInternetConnected() {
        if(mConnectionDetector.isInternetConnected()){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public MySharedPreferences getMySharedPreferences() {
        return mySharedPreferences;
    }


}
