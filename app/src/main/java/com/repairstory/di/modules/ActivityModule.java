package com.repairstory.di.modules;

import android.app.Activity;


import com.repairstory.controller.IDataManager;
import com.repairstory.di.annotations.PerActivity;

import com.repairstory.ui.addlocation.AddLocationMvpPresenter;
import com.repairstory.ui.addlocation.AddLocationMvpView;
import com.repairstory.ui.addlocation.AddLocationPresenter;
import com.repairstory.ui.createstory.CreateEditStoryMvpPresenter;
import com.repairstory.ui.createstory.CreateEditStoryMvpView;
import com.repairstory.ui.createstory.CreateEditStoryPresenter;
import com.repairstory.ui.home.HomeMvpPresenter;
import com.repairstory.ui.home.HomeMvpView;
import com.repairstory.ui.login.LoginMvpPresenter;
import com.repairstory.ui.login.LoginMvpView;
import com.repairstory.ui.login.LoginPresenter;
import com.repairstory.ui.profile.ProfileMvpPresenter;
import com.repairstory.ui.profile.ProfileMvpView;
import com.repairstory.ui.profile.ProfilePresenter;
import com.repairstory.ui.signup.SignupMvpPresenter;
import com.repairstory.ui.signup.SignupMvpView;
import com.repairstory.ui.signup.SignupPresenter;
import com.repairstory.ui.home.HomePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ilkay on 10/03/2017.
 */

@Module
public class ActivityModule {
    Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    Activity provideActivity() {
        return activity;
    }



    @Provides
    @PerActivity
    LoginMvpPresenter<LoginMvpView> providesLoginPresenter(IDataManager IDataManager) {
        return new LoginPresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    SignupMvpPresenter<SignupMvpView> providesSignupPresenter(IDataManager IDataManager) {
        return new SignupPresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    HomeMvpPresenter<HomeMvpView> providesSuperVisorHomePresenter(IDataManager IDataManager) {
        return new HomePresenter<>(IDataManager);
    }


    @Provides
    @PerActivity
    ProfileMvpPresenter<ProfileMvpView> providesProfilePresenter(IDataManager IDataManager) {
        return new ProfilePresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    CreateEditStoryMvpPresenter<CreateEditStoryMvpView> providesCreateStoryPresenter(IDataManager IDataManager) {
        return new CreateEditStoryPresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    AddLocationMvpPresenter<AddLocationMvpView> providesAddLocationPresenter(IDataManager IDataManager) {
        return new AddLocationPresenter<>(IDataManager);
    }
}
