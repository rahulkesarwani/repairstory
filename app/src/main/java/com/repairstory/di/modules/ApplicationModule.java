package com.repairstory.di.modules;

import android.app.Application;
import android.content.Context;

import com.repairstory.controller.DataManager;
import com.repairstory.controller.IDataManager;
import com.repairstory.controller.api.ApiHelper;
import com.repairstory.controller.api.IApiHelper;
import com.repairstory.controller.pref.IPreferenceHelper;
import com.repairstory.controller.pref.PreferenceHelper;
import com.repairstory.di.annotations.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by rahul on 09/03/2017.
 */

@Module
public class ApplicationModule {
	
	private final Application app;

	public ApplicationModule(Application app) {
		this.app = app;
	}
	
	@Provides
	@ApplicationContext
    Context provideContext() {
		return app;
	}
	
	@Provides
    Application provideApplication(){
		return app;
	}
	
	@Provides
	@Singleton
	IDataManager provideDataManager(@ApplicationContext Context context, IPreferenceHelper mIPreferenceHelper, IApiHelper mIApiHelper) {
		return new DataManager( context, mIPreferenceHelper, mIApiHelper);
	}

	/*@Provides
	@Singleton
	IDbHelper provideDbHelper(DatabaseManager databaseManager) {
		return new DbHelper(databaseManager);
	}*/


	@Provides
	@Singleton
	IPreferenceHelper providePreferencesHelper(@ApplicationContext Context context) {
		return new PreferenceHelper(context);
	}
	
	@Provides
	@Singleton
	IApiHelper provideApiHelper(@ApplicationContext Context context) {
		return new ApiHelper(context);
	}
	
}
