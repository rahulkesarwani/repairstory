/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.repairstory.di.components;



import com.repairstory.baseactivities.BaseActivity;
import com.repairstory.di.annotations.PerActivity;
import com.repairstory.di.modules.ActivityModule;
import com.repairstory.ui.addlocation.AddLocationActivity;
import com.repairstory.ui.createstory.CreateEditStoryActivity;
import com.repairstory.ui.login.LoginActivity;
import com.repairstory.ui.profile.ProfileActivity;
import com.repairstory.ui.signup.SignupActivity;
import com.repairstory.ui.home.HomeActivity;

import dagger.Component;

/**
 * Created by iaktas on 24/04/17.
 */


@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {


    void inject(BaseActivity baseActivity);

    void inject(LoginActivity activity);

    void inject(SignupActivity activity);

    void inject(HomeActivity activity);

    void inject(ProfileActivity profileActivity);

    void inject(CreateEditStoryActivity createStoryActivity);

    void inject(AddLocationActivity addLocationActivity);
}
