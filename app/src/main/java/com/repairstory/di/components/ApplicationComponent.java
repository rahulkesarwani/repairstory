package com.repairstory.di.components;


import android.app.Application;
import android.content.Context;

import com.repairstory.MyApp;
import com.repairstory.controller.IDataManager;
import com.repairstory.di.annotations.ApplicationContext;
import com.repairstory.di.modules.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ilkay on 26/02/2017.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(MyApp app);


    @ApplicationContext
    Context context();
    
    Application application();

    IDataManager getDataManager();

}
