package com.repairstory.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;

@SuppressWarnings("deprecation")
public class ConnectionDetector {

    private Context _context;
    private static ConnectionDetector instance = null;

    public ConnectionDetector(Context context) {
        this._context = context;
    }

    public static ConnectionDetector getInstance(Context context){
        if (instance != null ){
            return instance;
        }else {
            instance = new ConnectionDetector(context);
            return instance;
        }
    }

    public boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    return true;
                }
            }
        } else {

            if (connectivityManager != null) {
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null)
                    for (int i = 0; i < info.length; i++)
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
            }
        }
        return false;
    }

    public boolean isInternetConnected() {
        boolean connectionFlag = false;
        ConnectivityManager mConnectivityManager = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = mConnectivityManager.getActiveNetworkInfo();
        connectionFlag = networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected() == true ? true : false;
        return connectionFlag;
    }


}