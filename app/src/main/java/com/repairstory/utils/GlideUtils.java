package com.repairstory.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;


public class GlideUtils {
    public GlideUtils() {
    }

    public static void loadImage(Context context, String url, ImageView viewId, int placeHolder) {
        if (placeHolder == 0) {
            Glide.with(context).setDefaultRequestOptions((new RequestOptions()).fitCenter().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate()).load(url).into(viewId);
        } else {
            Glide.with(context).setDefaultRequestOptions((new RequestOptions()).fitCenter().placeholder(placeHolder).diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate()).load(url).into(viewId);
        }

    }

    public static void loadImageWithNoCache(Context context, String url, ImageView viewId, int placeHolder) {
        if (placeHolder == 0) {
            Glide.with(context).setDefaultRequestOptions((new RequestOptions()).fitCenter().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).dontAnimate()).load(url).into(viewId);
        } else {
            Glide.with(context).setDefaultRequestOptions((new RequestOptions()).fitCenter().placeholder(placeHolder).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).dontAnimate()).load(url).into(viewId);
        }

    }

    public static void loadImage(Context context, String url, ImageView viewId, int placeHolder, boolean fadeIn) {
        Glide.with(context).setDefaultRequestOptions((new RequestOptions()).placeholder(placeHolder).diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate()).load(url).into(viewId);
    }

    public static void loadImage(Context context, String url, ImageView viewId, int placeHolder, String centreCrop) {
        Glide.with(context).setDefaultRequestOptions((new RequestOptions()).placeholder(placeHolder).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)).load(url).into(viewId);
    }

    public static void loadImageWithoutPlaceholder(Context context, String url, ImageView viewId) {
        Glide.with(context).setDefaultRequestOptions((new RequestOptions()).diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate()).load(url).into(viewId);
    }

    public static void loadImageWithCornerRadius(Context context, String url, ImageView viewId) {

        Glide.with(context).setDefaultRequestOptions((new RequestOptions()).diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate()).load(url).into(viewId);
    }
}
