package com.repairstory.utils.customviews;

/**
 * Created by rahul on 10/4/17.
 */


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;

import com.repairstory.R;


public class CustomButton extends AppCompatButton {
    private static final String TAG = AppCompatButton.class.getCanonicalName();

    public CustomButton(Context context, String typeface) {
        super(context);
       setCustomFont(context,typeface);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        String customFont = a.getString(R.styleable.CustomTextView_customFont);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    public boolean setCustomFont(Context ctx, String asset) {
        Typeface tf = null;

        switch (asset){
            case "bold":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/bold.ttf");
                break;
            case "light":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/light.ttf");
                break;
            case "medium":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/extrabold.ttf");
                break;
            case "regular":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/regular.ttf");
                break;
            case "semibold":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/semibold.ttf");
                break;
        }

        setTypeface(tf);
        return true;

    }

}