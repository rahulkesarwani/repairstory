package com.repairstory.utils.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.google.android.material.textfield.TextInputEditText;
import com.repairstory.R;


/**
 * Created by rahul on 11/4/17.
 */

public class CustomEditText extends TextInputEditText {
    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context,attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context,attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        String customFont = a.getString(R.styleable.CustomTextView_customFont);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    public boolean setCustomFont(Context ctx, String asset) {
        Typeface tf = null;

        switch (asset){
            case "bold":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/bold.ttf");
                break;
            case "light":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/light.ttf");
                break;
            case "medium":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/extrabold.ttf");
                break;
            case "regular":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/regular.ttf");
                break;
            case "semibold":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/semibold.ttf");
                break;
            case "nu_regular":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/NunitoSans-Regular.ttf");
                break;
            case "nu_bold":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/NunitoSans-Bold.ttf");
                break;
        }
        setTypeface(tf);
        return true;
    }
}
