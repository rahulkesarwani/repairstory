package com.repairstory.utils;

/**
 * Created by rahul on 1/10/2018.
 */

public class CheckNullable {


    public static String checkNullable(String data) {

        if (data == null) {
            return "";
        } else {
            return data;
        }
    }


    public static Integer checkNullable(Integer data) {

        if (data == null) {
            return 0;
        } else {
            return data;
        }
    }

    public static Double checkNullable(Double data) {

        if (data == null) {
            return 0.0;
        } else {
            return data;
        }
    }

    public static Boolean checkNullable(Boolean data) {

        if (data == null) {
            return false;
        } else {
            return data;
        }
    }
}
