package com.repairstory.utils;

/**
 * Created by rahul on 1/5/2018.
 */

public interface AppConstants {

    /*Urls */

    String mBaseUrl = "http://138.197.165.248/api/";

    /**/
    String ADDRESS = "CURRENT_ADDRESS";
    String LATITUDE = "CURRENT_LATITUDE";
    String LONGITUDE = "CURRENT_LONGITUDE";
    String ACCESS_TOKEN = "ACCESS_TOKEN";

    /*for refresh token*/
    String FIRST_TIME_LOGIN_KEY = "firstTimeLoginTime";
    String CLIENT_ID = "client_id";
    String CLIENT_SECRET = "client_secret";
    String TOKEN_EXPIRY_TIME = "expires_in";
    String REFRESH_TOKEN = "refresh_token";
    String LOGIN_DATA = "LOGIN_DATA";

    String CHANNEL_ID = "crossing_guard";

    String STORY_TYPE = "STORY_TYPE";
    String OPEN_STORY_FRAGMENT = "OPEN_STORY_FRAGMENT";
    String COMPLETD_STORY_FRAGMENT = "COMPLETD_STORY_FRAGMENT";
    String HOME_FRAGMENT = "HOME_FRAGMENT";
    String CURRENT_STEP = "CURRENT_STEP";
    String STORY_ID = "STORY_ID";
    String MODE_TYPE = "MODE_TYPE";
    String LOCATION_DATA = "LOCATION_DATA";
    String CURRENT_MODE = "CURRENT_MODE";
    String TOTAL_OPEN_STORY ="TOTAL_OPEN_STORY" ;
    String TOTAL_COMPLETED_STORY ="TOTAL_COMPLETED_STORY" ;

    interface MODE_TYPE_CONSTANTS {
        String CREATE = "create";
        String EDIT = "edit";
    }
    interface STORY_TYPE_CONSTANTS {
        String OPEN = "open";
        String COMPLETED = "completed";
    }

    interface STORY_STEP {
        String step1 = "1";
        String step2 = "2";
        String step3 = "3";
        String testimonial = "testimonial";
    }


}
