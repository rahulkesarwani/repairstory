package com.repairstory.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.repairstory.models.logins.Datum;
import com.repairstory.ui.login.LoginActivity;
import com.vikktorn.picker.City;
import com.vikktorn.picker.State;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Response;

public class Helper {

    public static void handleErrorBody(Context context, String errorResponse) {

        try {
            JSONObject jObjError = new JSONObject(errorResponse);
            MessagesUtils.showToastError(context, jObjError.getString("message"));
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public static String getAccesstoken(Context context) {
        MySharedPreferences mySharedPreferences = new MySharedPreferences(context);
        String accessToken = mySharedPreferences.getString(AppConstants.ACCESS_TOKEN, "");
        return "Bearer " + accessToken;
    }

    public static String convertDateFormmat(String dateString, String givenFormat, String requiredFormat) {
        String result = "";
        SimpleDateFormat simpleDateFormat1 = null; //"2017-02-21" //"yyyy-MM-dd"
        SimpleDateFormat simpleDateFormat2 = null;
        Date tempDate = null;

        try {
            simpleDateFormat1 = new SimpleDateFormat(givenFormat);
            simpleDateFormat2 = new SimpleDateFormat(requiredFormat);

            tempDate = simpleDateFormat1.parse(dateString);
            result = simpleDateFormat2.format(tempDate);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String hasErrorBody(Response response) {
        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }
        return errorResponse;
    }

    public static boolean isImageFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.startsWith("image");
    }

    public static boolean isVideoFile(String path) {
        String mimeType = URLConnection.guessContentTypeFromName(path);
        return mimeType != null && mimeType.startsWith("video");
    }

    public static void showConfirmationDialog(Context context, String message, DialogCLick mListener) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        mListener.onYesClicked();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onNoClicked();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    public interface DialogCLick {
        void onYesClicked();

        void onNoClicked();
    }


    public static boolean isProfileCompleted(MySharedPreferences mySharedPreferences) {

        Datum userData = (Datum) mySharedPreferences.getObj(AppConstants.LOGIN_DATA, Datum.class);

        if (TextUtils.isEmpty(userData.getLogo())) {
            return false;
        } else if (TextUtils.isEmpty(userData.getName())) {
            return false;
        } else if (TextUtils.isEmpty(userData.getEmail())) {
            return false;
        } else if (TextUtils.isEmpty(userData.getPhone())) {
            return false;
        } else if (TextUtils.isEmpty(userData.getWebsite())) {
            return false;
        } else if (TextUtils.isEmpty(userData.getAddress())) {
            return false;
        } else if (TextUtils.isEmpty(userData.getCountry())) {
            return false;
        } else if (TextUtils.isEmpty(userData.getState())) {
            return false;
        } else if (TextUtils.isEmpty(userData.getCity())) {
            return false;
        } else if (TextUtils.isEmpty(userData.getLocality())) {
            return false;
        } else if (TextUtils.isEmpty(userData.getSubLocalityAddress())) {
            return false;
        } else if (TextUtils.isEmpty(userData.getCurrency())) {
            return false;
        } else if (TextUtils.isEmpty(userData.getBio())) {
            return false;
        } else if (userData.getServicesTags().size() == 0) {
            return false;
        } else
            return true;

    }


    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(source);
        }
    }


    public static void showSuccessDialog(Context context, String message, ConfirmClickListener mListener) {

        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setTitleText("Success!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        sweetAlertDialog.dismissWithAnimation();
                        if (mListener == null)
                            return;
                        mListener.onConfirmClicked();
                    }
                })
                .setContentText(message)
                .show();

    }


    public static void showErrorDialog(Context context, String message, ConfirmClickListener mListener) {

        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setTitleText("Error!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        sweetAlertDialog.dismissWithAnimation();
                        if (mListener == null)
                            return;
                        mListener.onConfirmClicked();
                    }
                })
                .setContentText(message)
                .show();

    }

    public interface ConfirmClickListener {
        public void onConfirmClicked();
    }
}
