package com.repairstory.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Set;

/**
 * Created by Rahull on 7/4/17.
 */

public class MySharedPreferences {

    private Context context;
    private final String SHAREDPREFERENCE = "mSharedPreference";
    private SharedPreferences mSharedPreference;
    /*    private LocationSaveModel savedObject;*/
    private Gson gson = new GsonBuilder().create();
    private static MySharedPreferences mySharedPreferences = null;

    public MySharedPreferences(Context context) {
        this.context = context;
    }

    public static MySharedPreferences getInstance(Context context) {
        if (mySharedPreferences == null)
            mySharedPreferences = new MySharedPreferences(context);

        return mySharedPreferences;
    }

    public void putString(String key, String value) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        mSharedPreference.edit().putString(key, value).commit();
    }

    public void putBoolean(String key, Boolean value) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        mSharedPreference.edit().putBoolean(key, value).commit();
    }

    public void putInt(String key, Integer value) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        mSharedPreference.edit().putInt(key, value).commit();
    }

    public void putFloat(String key, Float value) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        mSharedPreference.edit().putFloat(key, value).commit();
    }

    public void putLong(String key, Long value) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        mSharedPreference.edit().putLong(key, value).commit();
    }

    public String getString(String key) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        return mSharedPreference.getString(key, null);
    }

    public boolean getBoolean(String key) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        return mSharedPreference.getBoolean(key, false);
    }

    public Integer getInt(String key) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        return mSharedPreference.getInt(key, 0);
    }

    public Long getLong(String key) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        return mSharedPreference.getLong(key, 0L);
    }

    public Float getFloat(String key) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        return mSharedPreference.getFloat(key, 0.0F);
    }

    public String getString(String key, String defaultvalue) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        return mSharedPreference.getString(key, defaultvalue);
    }

    public boolean getBoolean(String key, boolean defaultvalue) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        return mSharedPreference.getBoolean(key, defaultvalue);
    }

    public Integer getInt(String key, Integer defaultvalue) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        return mSharedPreference.getInt(key, defaultvalue);
    }

    public Long getLong(String key, Long defaultvalue) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        return mSharedPreference.getLong(key, defaultvalue);
    }

    public Float getFloat(String key, Float defaultvalue) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        return mSharedPreference.getFloat(key, defaultvalue);
    }


    public void deleteAllKeyFromPreference() {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        mSharedPreference.edit().clear().commit();
    }

    public void deleteSingleKey(String key) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        mSharedPreference.edit().remove(key).commit();
    }

    public boolean isExistKey(String key) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        if (mSharedPreference.contains(key))
            return true;
        else
            return false;
    }

    public Set<String> getStringSet(String key) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        return mSharedPreference.getStringSet(key, null);
    }

    public void putStringSet(String key, Set<String> value) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        mSharedPreference.edit().putStringSet(key, value).commit();
    }

    public void setObj(String key, Object obj) {

        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        if (obj == null) {
            mSharedPreference.edit().putString(key, "").commit();
        } else {
            mSharedPreference.edit().putString(key, gson.toJson(obj)).commit();
        }

    }

    public Object getObj(String key, Class<?> object) {
        mSharedPreference = context.getSharedPreferences(SHAREDPREFERENCE, context.MODE_PRIVATE);
        String savedValue = mSharedPreference.getString(key, null);
        if (savedValue == null) {
            return null;
        } else {
            return gson.fromJson(savedValue, object);
        }
    }

}
