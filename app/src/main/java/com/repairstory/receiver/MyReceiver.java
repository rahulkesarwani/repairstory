package com.repairstory.receiver;

import android.content.Context;
import android.util.Log;

import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadServiceBroadcastReceiver;

import org.greenrobot.eventbus.EventBus;

import io.reactivex.disposables.CompositeDisposable;

public class MyReceiver extends UploadServiceBroadcastReceiver {
    private String TAG = MyReceiver.class.getSimpleName();


    @Override
    public void onProgress(Context context, UploadInfo uploadInfo) {

        Log.d(TAG, "onProgress: " + uploadInfo.getUploadId() + "   >>>>>" + uploadInfo.getProgressPercent());
        try {
//            EventBus.getDefault().postSticky(model);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onError(Context context, UploadInfo uploadInfo, ServerResponse serverResponse, Exception exception) {

        try {
            Log.d(TAG, "onError: "+serverResponse.getBodyAsString());
//             EventBus.getDefault().postSticky(model);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCompleted(Context context, UploadInfo uploadInfo, ServerResponse serverResponse) {

        try {
            Log.d(TAG, "onCompleted: "+serverResponse.getBodyAsString());
            // your implementation
            Log.d(TAG, "onCompleted: " + uploadInfo.getUploadId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //for refreshing media on create/ update story with server url
        EventBus.getDefault().postSticky(true);
    }

    @Override
    public void onCancelled(Context context, UploadInfo uploadInfo) {

        try {

            // your implementation
            Log.d(TAG, "onCancelled: " + uploadInfo.getUploadId());

        } catch (Exception e) {
        }
    }
}