package com.repairstory.baseactivities.base;


import com.repairstory.controller.IDataManager;

/**
 * Created by dell on 6/6/2018.
 */

public class BasePresenter <V extends MvpView> implements MvpPresenter<V> {

    private final IDataManager IDataManager;

    private V mMvpView;

    public BasePresenter(com.repairstory.controller.IDataManager IDataManager) {
        this.IDataManager = IDataManager;
    }

    @Override
    public void onAttach(V mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mMvpView = null;
    }

    @Override
    public void onTokenReceived() {

    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    public V getMvpView() {
        return mMvpView;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new MvpViewNotAttachedException();
    }

    public IDataManager getIDataManager() {
        return IDataManager;
    }

    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter.onAttach(MvpView) before" +
                    " requesting data to the Presenter");
        }
    }
}
