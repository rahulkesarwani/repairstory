package com.repairstory.models.storycount;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StoryCount implements Parcelable
{

@SerializedName("total_open")
@Expose
private Integer totalOpen;
@SerializedName("total_completed")
@Expose
private Integer totalCompleted;
public final static Parcelable.Creator<StoryCount> CREATOR = new Creator<StoryCount>() {


@SuppressWarnings({
"unchecked"
})
public StoryCount createFromParcel(Parcel in) {
return new StoryCount(in);
}

public StoryCount[] newArray(int size) {
return (new StoryCount[size]);
}

}
;

protected StoryCount(Parcel in) {
this.totalOpen = ((Integer) in.readValue((Integer.class.getClassLoader())));
this.totalCompleted = ((Integer) in.readValue((Integer.class.getClassLoader())));
}

public StoryCount() {
}

public Integer getTotalOpen() {
return totalOpen;
}

public void setTotalOpen(Integer totalOpen) {
this.totalOpen = totalOpen;
}

public Integer getTotalCompleted() {
return totalCompleted;
}

public void setTotalCompleted(Integer totalCompleted) {
this.totalCompleted = totalCompleted;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(totalOpen);
dest.writeValue(totalCompleted);
}

public int describeContents() {
return 0;
}

}