package com.repairstory.models.storycount;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StoryCountModel implements Parcelable {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private StoryCount data;
    public final static Parcelable.Creator<StoryCountModel> CREATOR = new Creator<StoryCountModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public StoryCountModel createFromParcel(Parcel in) {
            return new StoryCountModel(in);
        }

        public StoryCountModel[] newArray(int size) {
            return (new StoryCountModel[size]);
        }

    };

    protected StoryCountModel(Parcel in) {
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.success = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.data = ((StoryCount) in.readValue((StoryCount.class.getClassLoader())));
    }

    public StoryCountModel() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public StoryCount getData() {
        return data;
    }

    public void setData(StoryCount data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(success);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

}