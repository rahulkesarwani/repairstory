package com.repairstory.models.story;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StoryModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("device_details")
    @Expose
    private String deviceDetails;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("progress_details")
    @Expose
    private String progressDetails;
    @SerializedName("final_repair_update")
    @Expose
    private String finalRepairUpdate;
    @SerializedName("media")
    @Expose
    private Media media;
    @SerializedName("step")
    @Expose
    private String step;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("testimonial")
    @Expose
    private String testimonial;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("cost")
    @Expose
    private String cost;

    public final static Parcelable.Creator<StoryModel> CREATOR = new Creator<StoryModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public StoryModel createFromParcel(Parcel in) {
            return new StoryModel(in);
        }

        public StoryModel[] newArray(int size) {
            return (new StoryModel[size]);
        }

    };

    protected StoryModel(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.categoryId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.deviceDetails = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.progressDetails = ((String) in.readValue((String.class.getClassLoader())));
        this.finalRepairUpdate = ((String) in.readValue((String.class.getClassLoader())));
        this.media = ((Media) in.readValue((Media.class.getClassLoader())));
        this.step = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.testimonial = ((String) in.readValue((String.class.getClassLoader())));
        this.duration = ((String) in.readValue((String.class.getClassLoader())));
        this.cost = ((String) in.readValue((String.class.getClassLoader())));
    }

    public StoryModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getDeviceDetails() {
        return deviceDetails;
    }

    public void setDeviceDetails(String deviceDetails) {
        this.deviceDetails = deviceDetails;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProgressDetails() {
        return progressDetails;
    }

    public void setProgressDetails(String progressDetails) {
        this.progressDetails = progressDetails;
    }

    public String getFinalRepairUpdate() {
        return finalRepairUpdate;
    }

    public void setFinalRepairUpdate(String finalRepairUpdate) {
        this.finalRepairUpdate = finalRepairUpdate;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getTestimonial() {
        return testimonial;
    }

    public void setTestimonial(String testimonial) {
        this.testimonial = testimonial;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(userId);
        dest.writeValue(categoryId);
        dest.writeValue(deviceDetails);
        dest.writeValue(description);
        dest.writeValue(progressDetails);
        dest.writeValue(finalRepairUpdate);
        dest.writeValue(media);
        dest.writeValue(step);
        dest.writeValue(status);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(testimonial);
        dest.writeValue(duration);
        dest.writeValue(cost);
    }

    public int describeContents() {
        return 0;
    }

}