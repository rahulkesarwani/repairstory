package com.repairstory.models.story;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateUpdateStoryModel implements Serializable
{

    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("device_details")
    @Expose
    private String deviceDetails;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("progress_details")
    @Expose
    private String progressDetails;
    @SerializedName("final_repair_update")
    @Expose
    private String finalRepairUpdate;
    @SerializedName("step")
    @Expose
    private String step;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("media_step")
    @Expose
    private String mediaStep;
    @SerializedName("testimonial")
    @Expose
    private String testimonial;
    @SerializedName("cost")
    @Expose
    private String cost;
    @SerializedName("duration")
    @Expose
    private String duration;
    private final static long serialVersionUID = -1864408276339279100L;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getDeviceDetails() {
        return deviceDetails;
    }

    public void setDeviceDetails(String deviceDetails) {
        this.deviceDetails = deviceDetails;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProgressDetails() {
        return progressDetails;
    }

    public void setProgressDetails(String progressDetails) {
        this.progressDetails = progressDetails;
    }

    public String getFinalRepairUpdate() {
        return finalRepairUpdate;
    }

    public void setFinalRepairUpdate(String finalRepairUpdate) {
        this.finalRepairUpdate = finalRepairUpdate;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMediaStep() {
        return mediaStep;
    }

    public void setMediaStep(String mediaStep) {
        this.mediaStep = mediaStep;
    }

    public String getTestimonial() {
        return testimonial;
    }

    public void setTestimonial(String testimonial) {
        this.testimonial = testimonial;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}