package com.repairstory.models.story;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pics implements Parcelable {

    @SerializedName("step_1")
    @Expose
    private List<String> step1 = new ArrayList<>();
    @SerializedName("step_2")
    @Expose
    private List<String> step2 = new ArrayList<>();
    @SerializedName("step_3")
    @Expose
    private List<String> step3 = new ArrayList<>();
    @SerializedName("testimonial")
    @Expose
    private List<String> testimonial = new ArrayList<>();
    public final static Parcelable.Creator<Pics> CREATOR = new Creator<Pics>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Pics createFromParcel(Parcel in) {
            return new Pics(in);
        }

        public Pics[] newArray(int size) {
            return (new Pics[size]);
        }

    };

    protected Pics(Parcel in) {
        in.readList(this.step1, (java.lang.String.class.getClassLoader()));
        in.readList(this.step2, (java.lang.String.class.getClassLoader()));
        in.readList(this.step3, (java.lang.String.class.getClassLoader()));
        in.readList(this.testimonial, (java.lang.String.class.getClassLoader()));
    }

    public Pics() {
    }

    public List<String> getStep1() {
        return step1;
    }

    public void setStep1(List<String> step1) {
        this.step1 = step1;
    }

    public List<String> getStep2() {
        return step2;
    }

    public void setStep2(List<String> step2) {
        this.step2 = step2;
    }

    public List<String> getStep3() {
        return step3;
    }

    public void setStep3(List<String> step3) {
        this.step3 = step3;
    }

    public List<String> getTestimonial() {
        return testimonial;
    }

    public void setTestimonial(List<String> testimonial) {
        this.testimonial = testimonial;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(step1);
        dest.writeList(step2);
        dest.writeList(step3);
        dest.writeList(testimonial);
    }

    public int describeContents() {
        return 0;
    }

}
