package com.repairstory.models.story;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Serializable {

    @SerializedName("story_id")
    @Expose
    private Integer storyId;
    @SerializedName("last_page")
    @Expose
    private Integer lastPage;
    @SerializedName("data")
    @Expose
    private List<StoryModel> stories = null;
    public final static Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        public Data[] newArray(int size) {
            return (new Data[size]);
        }

    };

    protected Data(Parcel in) {
        this.storyId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.lastPage = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.stories, (StoryModel.class.getClassLoader()));
    }

    public Data() {
    }

    public Integer getLastPage() {
        return lastPage;
    }

    public void setLastPage(Integer lastPage) {
        this.lastPage = lastPage;
    }

    public List<StoryModel> getStories() {
        return stories;
    }

    public void setStories(List<StoryModel> stories) {
        this.stories = stories;
    }



    public Integer getStoryId() {
        return storyId;
    }

    public void setStoryId(Integer storyId) {
        this.storyId = storyId;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(storyId);
        dest.writeValue(lastPage);
        dest.writeList(stories);
    }

    public int describeContents() {
        return 0;
    }
}