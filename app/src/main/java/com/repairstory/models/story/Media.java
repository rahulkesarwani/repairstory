package com.repairstory.models.story;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Media implements Parcelable {

    @SerializedName("pics")
    @Expose
    private Pics pics;
    @SerializedName("videos")
    @Expose
    private Pics videos;
    public final static Parcelable.Creator<Media> CREATOR = new Creator<Media>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Media createFromParcel(Parcel in) {
            return new Media(in);
        }

        public Media[] newArray(int size) {
            return (new Media[size]);
        }

    };

    protected Media(Parcel in) {
        this.pics = ((Pics) in.readValue((Pics.class.getClassLoader())));
        this.videos = ((Pics) in.readValue((Pics.class.getClassLoader())));
    }

    public Media() {
    }

    public Pics getPics() {
        return pics;
    }

    public void setPics(Pics pics) {
        this.pics = pics;
    }

    public Pics getVideos() {
        return videos;
    }

    public void setVideos(Pics videos) {
        this.videos = videos;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(pics);
        dest.writeValue(videos);
    }

    public int describeContents() {
        return 0;
    }

}