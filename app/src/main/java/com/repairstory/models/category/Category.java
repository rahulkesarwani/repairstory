package com.repairstory.models.category;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category implements Parcelable
{

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("name")
@Expose
private String name;
@SerializedName("created_at")
@Expose
private String createdAt;
@SerializedName("updated_at")
@Expose
private String updatedAt;
public final static Parcelable.Creator<Category> CREATOR = new Creator<Category>() {


@SuppressWarnings({
"unchecked"
})
public Category createFromParcel(Parcel in) {
return new Category(in);
}

public Category[] newArray(int size) {
return (new Category[size]);
}

}
;

protected Category(Parcel in) {
this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
this.name = ((String) in.readValue((String.class.getClassLoader())));
this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
}

public Category() {
}

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public String getCreatedAt() {
return createdAt;
}

public void setCreatedAt(String createdAt) {
this.createdAt = createdAt;
}

public String getUpdatedAt() {
return updatedAt;
}

public void setUpdatedAt(String updatedAt) {
this.updatedAt = updatedAt;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(id);
dest.writeValue(name);
dest.writeValue(createdAt);
dest.writeValue(updatedAt);
}

public int describeContents() {
return 0;
}

}