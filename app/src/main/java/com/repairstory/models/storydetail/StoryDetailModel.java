package com.repairstory.models.storydetail;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.repairstory.models.story.StoryModel;

public class StoryDetailModel implements Parcelable {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private StoryModel data;
    public final static Parcelable.Creator<StoryDetailModel> CREATOR = new Creator<StoryDetailModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public StoryDetailModel createFromParcel(Parcel in) {
            return new StoryDetailModel(in);
        }

        public StoryDetailModel[] newArray(int size) {
            return (new StoryDetailModel[size]);
        }

    };

    protected StoryDetailModel(Parcel in) {
        this.success = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((StoryModel) in.readValue((StoryModel.class.getClassLoader())));
    }

    public StoryDetailModel() {
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StoryModel getData() {
        return data;
    }

    public void setData(StoryModel data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(success);
        dest.writeValue(message);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }

}