package com.repairstory.apis;

import com.repairstory.models.category.CategoryResponseModel;
import com.repairstory.models.logins.LoginSignupRequestModel;
import com.repairstory.models.logins.LoginResponseModel;
import com.repairstory.models.story.CreateUpdateStoryModel;
import com.repairstory.models.story.ListStoryModel;
import com.repairstory.models.story.SuccessModel;
import com.repairstory.models.storycount.StoryCountModel;
import com.repairstory.models.storydetail.StoryDetailModel;

import java.util.HashMap;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;


/**
 * Created by rahul on 28/6/17.
 */

public interface ApiInterface {


    @POST("auth/login")
    Observable<Response<LoginResponseModel>> hitLoginApi(@Body LoginSignupRequestModel loginRequestModel);

    @POST("auth/register")
    Observable<Response<LoginResponseModel>> hitSignupApi(@Body LoginSignupRequestModel loginRequestModel);

    @POST("me")
    Observable<Response<LoginResponseModel>> hitProfileUpdateApi(@Body MultipartBody request);

    @GET("stories")
    Observable<Response<ListStoryModel>> hitStoriesListApi(@QueryMap HashMap<String,String> requestMap);

    @POST("stories")
    Observable<Response<SuccessModel>> hitCreateNewStoryApi(@Body CreateUpdateStoryModel req);

    @GET("categories")
    Observable<Response<CategoryResponseModel>> getCategoriesApi();

    @PUT("stories/{id}")
    Observable<Response<SuccessModel>> hitUpdateStoryApi(@Path("id") Integer id, @Body CreateUpdateStoryModel req);

    @DELETE("stories/{id}")
    Observable<Response<SuccessModel>> hitDeleteStoryApi(@Path("id") int id);

    @GET("stories/{id}")
    Observable<Response<StoryDetailModel>> hitGetStoryApi(@Path("id") int id);

    @GET("stories")
    Observable<Response<StoryCountModel>> hitCountTotalStoryApi(@QueryMap HashMap<String,String> requestMap);

}