package com.repairstory.db.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class CheckInOutDBModel implements Parcelable {

    @NonNull
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    @Expose
    private Long id;


    @ColumnInfo(name = "eventType")
    @SerializedName("eventType")
    @Expose
    private String eventType;

    @ColumnInfo(name = "locationId")
    @SerializedName("locationId")
    @Expose
    private Integer locationId;

    @ColumnInfo(name = "createdAt")
    @SerializedName("createdAt")
    @Expose
    private Long createdAt;

    public final static Creator<CheckInOutDBModel> CREATOR = new Creator<CheckInOutDBModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CheckInOutDBModel createFromParcel(Parcel in) {
            return new CheckInOutDBModel(in);
        }

        public CheckInOutDBModel[] newArray(int size) {
            return (new CheckInOutDBModel[size]);
        }

    };

    protected CheckInOutDBModel(Parcel in) {
        this.id = ((Long) in.readValue((Long.class.getClassLoader())));
        this.eventType = ((String) in.readValue((String.class.getClassLoader())));
        this.locationId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdAt = ((Long) in.readValue((Long.class.getClassLoader())));
    }

    public CheckInOutDBModel() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(eventType);
        dest.writeValue(locationId);
        dest.writeValue(createdAt);
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setId(@NonNull Long id) {
        this.id = id;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    @NonNull
    public Long getId() {
        return id;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventType() {
        return eventType;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }
}
