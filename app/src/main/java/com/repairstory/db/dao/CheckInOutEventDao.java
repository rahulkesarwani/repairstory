package com.repairstory.db.dao;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.repairstory.db.entity.CheckInOutDBModel;
import java.util.List;
import io.reactivex.Maybe;

@Dao
public interface CheckInOutEventDao {


    @Query("DELETE FROM CheckInOutDBModel")
    public void nukeTable();

    @Insert
    long add(CheckInOutDBModel checkInOutDBModel);

    @Query("DELETE FROM CheckInOutDBModel where id=:id")
    void delete(long id);

    @Query("SELECT * FROM CheckInOutDBModel order by createdAt asc limit 1")
    Maybe<CheckInOutDBModel> getPendingTaskFromQueue();

    @Query("UPDATE CheckInOutDBModel SET createdAt = :updatedAt WHERE id = :id")
    void updateProcessingTime(long id, Long updatedAt);

    @Update
    void updateVideoEntity(CheckInOutDBModel videoEntityModel);

    @Query("SELECT * FROM CheckInOutDBModel where id =:id")
    Maybe<CheckInOutDBModel> getById(long id);

}
