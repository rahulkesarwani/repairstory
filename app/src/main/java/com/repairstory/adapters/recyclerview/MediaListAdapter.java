package com.repairstory.adapters.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.repairstory.R;
import com.repairstory.utils.GlideUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MediaListAdapter extends RecyclerView.Adapter {
    private final String TAG = MediaListAdapter.class.getCanonicalName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private Context mContext = null;
    private ArrayList<String> mediaList;
    private ArrayList<String> deletedServerMediaList = new ArrayList<>();

    public MediaListAdapter(Context mContext, ArrayList<String> mediaList) {

        this.mContext = mContext;
        this.mediaList = mediaList;

    }

    @Override
    public int getItemViewType(int position) {
        return mediaList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_media, parent, false);
            vh = new MediaVH(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof MediaVH) {

            GlideUtils.loadImage(mContext, mediaList.get(position), ((MediaVH) holder).mediaIV, R.color.project_one);
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return mediaList.size();
    }


    public class MediaVH extends RecyclerView.ViewHolder {
        @BindView(R.id.mediaIV)
        ImageView mediaIV;
        @BindView(R.id.deleteBtn)
        ImageView deleteBtn;
        @BindView(R.id.parentLL)
        RelativeLayout parentLL;


        public MediaVH(View v) {
            super(v);
            ButterKnife.bind(this, v);

            deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mediaList.get(getAdapterPosition()).startsWith("http")) {
                        deletedServerMediaList.add(mediaList.get(getAdapterPosition()));
                    }
                    mediaList.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                }
            });
        }

    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBarPagination)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }


    public ArrayList<String> getDeletedServerMediaList() {
        return deletedServerMediaList;
    }
}
