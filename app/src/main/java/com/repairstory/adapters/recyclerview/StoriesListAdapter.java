package com.repairstory.adapters.recyclerview;

import android.content.Context;
import android.service.autofill.UserData;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.repairstory.R;
import com.repairstory.models.logins.Datum;
import com.repairstory.models.story.Media;
import com.repairstory.models.story.StoryModel;
import com.repairstory.utils.AppConstants;
import com.repairstory.utils.CheckNullable;
import com.repairstory.utils.GlideUtils;
import com.repairstory.utils.Helper;
import com.repairstory.utils.MySharedPreferences;
import com.repairstory.utils.customviews.CustomTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoriesListAdapter extends RecyclerView.Adapter {
    private final String TAG = StoriesListAdapter.class.getCanonicalName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;


    private Context mContext = null;
    private ArrayList<StoryModel> list;
    private String storyType;
    private RowClickListener mListener;
    private Datum userData;

    public StoriesListAdapter(Context mContext, ArrayList<StoryModel> list, String storyType, RowClickListener mListener) {

        this.mContext = mContext;
        this.list = list;
        this.storyType = storyType;
        this.mListener = mListener;
        userData = (Datum) MySharedPreferences.getInstance(mContext).getObj(AppConstants.LOGIN_DATA, Datum.class);
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_stories_list, parent, false);
            vh = new StoryVH(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof StoryVH) {

            if (storyType.equalsIgnoreCase(AppConstants.STORY_TYPE_CONSTANTS.OPEN)) {
                ((StoryVH) holder).deleteBtn.setVisibility(View.VISIBLE);
                String openDate = Helper.convertDateFormmat(CheckNullable.checkNullable(list.get(position).getCreatedAt()), "yyyy-MM-dd HH:mm:ss", "dd-MM-yyyy");
                ((StoryVH) holder).dateTV.setText(openDate);
                ((StoryVH) holder).completedStoryDataLL.setVisibility(View.GONE);

            } else {
                ((StoryVH) holder).deleteBtn.setVisibility(View.INVISIBLE);
                String closedDate = Helper.convertDateFormmat(CheckNullable.checkNullable(list.get(position).getUpdatedAt()), "yyyy-MM-dd HH:mm:ss", "dd-MM-yyyy");
                ((StoryVH) holder).dateTV.setText(closedDate);
                ((StoryVH) holder).completedStoryDataLL.setVisibility(View.VISIBLE);
            }

            String title = "#" + list.get(position).getId() + " - " + list.get(position).getDeviceDetails();
            ((StoryVH) holder).storyTitleTV.setText(title);
            ((StoryVH) holder).descriptionTV.setText(CheckNullable.checkNullable(list.get(position).getDescription()));

            ((StoryVH) holder).durationTV.setText(CheckNullable.checkNullable(list.get(position).getDuration()));
            ((StoryVH) holder).costTV.setText(CheckNullable.checkNullable(userData.getCurrency()) + " " + CheckNullable.checkNullable(list.get(position).getCost()));

            String mediaUrl = getSingleMediaData(list.get(position).getMedia());
            if (TextUtils.isEmpty(mediaUrl)) {
                ((StoryVH) holder).mediaIV.setVisibility(View.GONE);
            } else {
                ((StoryVH) holder).mediaIV.setVisibility(View.VISIBLE);
                GlideUtils.loadImageWithCornerRadius(mContext, mediaUrl, ((StoryVH) holder).mediaIV);
            }

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class StoryVH extends RecyclerView.ViewHolder {
        @BindView(R.id.storyTitleTV)
        TextView storyTitleTV;
        @BindView(R.id.descriptionTV)
        TextView descriptionTV;
        @BindView(R.id.durationTxt)
        TextView durationTxt;
        @BindView(R.id.durationTV)
        TextView durationTV;
        @BindView(R.id.costTV)
        TextView costTV;
        @BindView(R.id.completedStoryDataLL)
        RelativeLayout completedStoryDataLL;
        @BindView(R.id.mediaIV)
        ImageView mediaIV;
        @BindView(R.id.topRL)
        RelativeLayout topRL;
        @BindView(R.id.ic_watch)
        ImageView icWatch;
        @BindView(R.id.dateTV)
        TextView dateTV;
        @BindView(R.id.deleteBtn)
        ImageView deleteBtn;
        @BindView(R.id.parentLL)
        RelativeLayout parentLL;

        public StoryVH(View v) {
            super(v);
            ButterKnife.bind(this, v);

            deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onDeleteClicked(getAdapterPosition());
                }
            });

            parentLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (storyType.equalsIgnoreCase(AppConstants.STORY_TYPE_CONSTANTS.OPEN))
                        mListener.onRowClicked(getAdapterPosition());
                }
            });
        }

    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBarPagination)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public interface RowClickListener {
        void onRowClicked(int position);

        void onDeleteClicked(int position);
    }

    private String getSingleMediaData(Media media) {

        if (media.getPics().getStep1().size() > 0)
            return media.getPics().getStep1().get(0);
        else if (media.getPics().getStep2().size() > 0)
            return media.getPics().getStep2().get(0);
        else if (media.getPics().getStep3().size() > 0)
            return media.getPics().getStep3().get(0);
        else if (media.getVideos().getStep1().size() > 0)
            return media.getVideos().getStep1().get(0);
        else if (media.getVideos().getStep2().size() > 0)
            return media.getVideos().getStep2().get(0);
        else if (media.getVideos().getStep3().size() > 0)
            return media.getVideos().getStep3().get(0);
        else
            return "";
    }
}
